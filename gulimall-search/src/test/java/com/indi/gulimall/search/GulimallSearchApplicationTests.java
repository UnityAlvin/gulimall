package com.indi.gulimall.search;

import com.alibaba.fastjson.JSON;
import com.indi.gulimall.search.config.ElasticsearchConfig;
import lombok.Data;
import lombok.ToString;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms.Bucket;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.Avg;
import org.elasticsearch.search.aggregations.metrics.AvgAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.io.IOException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GulimallSearchApplicationTests {
    @Resource
    public RestHighLevelClient restHighLevelClient;

    @Test
    public void contextLoads() {
        System.out.println(restHighLevelClient);
    }

    @Data
    class User {
        private String name;
        private String email;
        private Integer age;
    }

    @Data
    @ToString
    public static class Account {
        private Integer accountNumber;
        private Integer balance;
        private String firstname;
        private String lastname;
        private Integer age;
        private String gender;
        private String address;
        private String employer;
        private String email;
        private String city;
        private String state;
    }

    /**
     * 测试保存索引
     *
     * @throws IOException
     */
    @Test
    public void testIndex() throws IOException {
        // 1. 创建索引请求
        IndexRequest indexRequest = new IndexRequest("user");

        // 2. 设置索引id
        indexRequest.id("1");

        // 3. 准备Json数据
        User user = new User();
        user.setName("nick");
        user.setEmail("nick@gmail.com");
        user.setAge(18);
        String jsonUser = JSON.toJSONString(user);

        // 4. 设置索引数据
        indexRequest.source(jsonUser, XContentType.JSON);

        // 5. 调用远程连接生成索引
        IndexResponse indexResponse = restHighLevelClient.index(indexRequest, ElasticsearchConfig.COMMON_OPTIONS);

        System.out.println(indexResponse);
    }

    /**
     * 搜索 address 中包含 mill 的所有人，以及他们的年龄分布、平均年龄、平均薪资
     *
     * @throws IOException
     */
    @Test
    public void searchData() throws IOException {
        // 1. 创建检索请求
        SearchRequest searchRequest = new SearchRequest();

        // 2. 为请求指定索引
        searchRequest.indices("bank");

        // 3. 构建检索条件
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.matchQuery("address", "mill"));
        searchSourceBuilder.aggregation(AggregationBuilders.terms("ageAgg").field("age").size(10));
        searchSourceBuilder.aggregation(AggregationBuilders.avg("balanceAvg").field("balance"));

        // 4. 为请求指定检索条件
        searchRequest.source(searchSourceBuilder);

        // 5. 执行检索请求、获得响应
        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, ElasticsearchConfig.COMMON_OPTIONS);

        // 6. 分析结果
        SearchHit[] hits = searchResponse.getHits().getHits();
        for (SearchHit hit : hits) {
            // 6.1 通过Json转换为实体类
            Account account = JSON.parseObject(hit.getSourceAsString(), Account.class);
            System.out.println(account);
        }

        // 6.2 获取聚合数据
        Aggregations aggs = searchResponse.getAggregations();
        Terms terms = aggs.get("ageAgg");
        terms.getBuckets().forEach(o -> System.out.println("年龄：" + o.getKeyAsString() + "\t人数：" + o.getDocCount()));

        Avg avg = aggs.get("balanceAvg");
        System.out.println("平均薪资" + avg.getValue());
    }

    /**
     * 按照年龄聚合，并且求这些年龄段的人的平均薪资
     */
    @Test
    public void test2() throws IOException {
        SearchRequest searchRequest = new SearchRequest();

        searchRequest.indices("bank");

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.matchAllQuery());
        searchSourceBuilder.aggregation(AggregationBuilders.terms("ageAgg").field("age").size(100)
                .subAggregation(AggregationBuilders.avg("balanceAvg").field("balance")));

        searchRequest.source(searchSourceBuilder);

        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, ElasticsearchConfig.COMMON_OPTIONS);

        Aggregations aggs = searchResponse.getAggregations();
        Terms terms = aggs.get("ageAgg");
        for (Bucket bucket : terms.getBuckets()) {
            int age = bucket.getKeyAsNumber().intValue();
            Aggregations subAggs = bucket.getAggregations();
            Avg avg = subAggs.get("balanceAvg");
            System.out.println("年龄：" + age + "\t平均薪资："+avg.getValue());
        }
    }

    /**
     * 查出所有年龄分布以及每个年龄段的平均薪资，并查出这些年龄段中M的平均薪资、F的平均薪资、
     */
    @Test
    public void test3() throws IOException {
        SearchRequest searchRequest = new SearchRequest();

        searchRequest.indices("bank");

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.matchAllQuery());
        TermsAggregationBuilder ageAggBuilder = AggregationBuilders.terms("ageAgg").field("age").size(100);
        TermsAggregationBuilder genderAggBuilder = AggregationBuilders.terms("genderAgg").field("gender.keyword");
        AvgAggregationBuilder genderBalanceAvgBuilder = AggregationBuilders.avg("genderBalanceAvg").field("balance");
        AvgAggregationBuilder ageBalanceAvgBuilder = AggregationBuilders.avg("ageBalanceAvg").field("balance");

        searchSourceBuilder.aggregation(ageAggBuilder.subAggregation(genderAggBuilder.subAggregation(genderBalanceAvgBuilder))
                .subAggregation(ageBalanceAvgBuilder));

        searchRequest.source(searchSourceBuilder);

        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, ElasticsearchConfig.COMMON_OPTIONS);
        Aggregations aggs = searchResponse.getAggregations();
        Terms terms = aggs.get("ageAgg");
        for (Bucket bucket : terms.getBuckets()) {
            Aggregations subAggs = bucket.getAggregations();
            Avg ageBalanceAvg = subAggs.get("ageBalanceAvg");
            System.out.println("年龄：" + bucket.getKeyAsString() + "\t人数：" + bucket.getDocCount() + "\t平均薪资："
                    + ageBalanceAvg.getValue());
            Terms genderAggTerms = subAggs.get("genderAgg");
            for (Bucket genderAggTermsBucket : genderAggTerms.getBuckets()) {
                Aggregations balanceAggs = genderAggTermsBucket.getAggregations();
                Avg genderBalanceAvgResp = balanceAggs.get("genderBalanceAvg");
                System.out.println("性别：" + genderAggTermsBucket.getKeyAsString() + "\t人数："
                        + genderAggTermsBucket.getDocCount() + "\t平均薪资：" + genderBalanceAvgResp.getValue());
            }
            System.out.println();
        }
    }
}
