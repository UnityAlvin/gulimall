package com.indi.gulimall.search.controller.admin;

import com.indi.common.enums.BizCodeEnum;
import com.indi.common.to.es.SkuEsModelTO;
import com.indi.common.utils.R;
import com.indi.gulimall.search.service.ProductSaveService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@Api(tags = "Es保存")
@RestController
@RequestMapping("/admin/search/save")
@Slf4j
public class EsSaveController {
    @Resource
    private ProductSaveService productSaveService;

    @ApiOperation("Es保存商品")
    @PostMapping("/product")
    public R productStatusUp(@RequestBody List<SkuEsModelTO> skuEsModelTOs){
        // 将商品服务传过来的 List<SkuEsModel> skuEsModels 存入到 es 中
        boolean flag = false;
        try {
            flag = productSaveService.productStatusUp(skuEsModelTOs);
        } catch (Exception e) {
            log.error("上架异常：{}",e);
            return R.error(BizCodeEnum.PRODUCT_UP_EXCEPTION);
        }
        if (flag) {
            return R.error(BizCodeEnum.PRODUCT_UP_EXCEPTION);
        }else{
            return R.ok();
        }
    }
}
