package com.indi.gulimall.search.service;

import com.indi.gulimall.search.vo.SearchParamVO;
import com.indi.gulimall.search.vo.SearchResultVO;

public interface MallSearchService {
    SearchResultVO search(SearchParamVO searchParamVO);
}
