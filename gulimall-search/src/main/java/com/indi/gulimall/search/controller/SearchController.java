package com.indi.gulimall.search.controller;

import com.indi.gulimall.search.service.MallSearchService;
import com.indi.gulimall.search.vo.SearchParamVO;
import com.indi.gulimall.search.vo.SearchResultVO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Controller
public class SearchController {
    @Resource
    private MallSearchService mallSearchService;

    @GetMapping("/list.html")
    public String list(SearchParamVO param, Model model,HttpServletRequest request){
        // 根据页面传过来的查询参数，去es中查询
        param.setQueryString(request.getQueryString());
        SearchResultVO result =  mallSearchService.search(param);
        model.addAttribute("result",result);
        return "list";
    }
}
