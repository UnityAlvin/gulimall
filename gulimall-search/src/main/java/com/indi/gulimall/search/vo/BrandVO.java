package com.indi.gulimall.search.vo;

import lombok.Data;

@Data
public class BrandVO {
    private Long brandId;
    private String name;
}
