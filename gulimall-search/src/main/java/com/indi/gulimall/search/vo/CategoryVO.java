package com.indi.gulimall.search.vo;

import lombok.Data;

@Data
public class CategoryVO {
    private Long catId;
    private String name;
}
