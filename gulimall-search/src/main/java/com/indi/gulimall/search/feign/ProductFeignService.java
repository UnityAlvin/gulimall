package com.indi.gulimall.search.feign;

import com.indi.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient("gulimall-product")
public interface ProductFeignService {
    @GetMapping("/admin/product/attr/info/{attrId}")
    R attrInfo(@PathVariable("attrId") Long attrId);

    @GetMapping("/admin/product/brand/list-by-ids")
    R brandListByIds(@RequestParam("brandIds") List<Long> brandIds);

    @GetMapping("/admin/product/category/info/{catId}")
    R categoryInfo(@PathVariable("catId") Long catId);
}
