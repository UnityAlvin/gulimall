package com.indi.gulimall.search.constant;

public class EsConstant {
    // 定义一个商品服务的常量，用来存储 sku 在 es 中的索引
     public static final String PRODUCT_INDEX = "gulimall_product";

     // 每页显示的商品个数
     public static final Integer PRODUCT_PAGE_SIZE = 5;
}
