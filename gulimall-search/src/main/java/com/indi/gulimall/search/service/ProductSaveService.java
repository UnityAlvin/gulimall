package com.indi.gulimall.search.service;

import com.indi.common.to.es.SkuEsModelTO;

import java.io.IOException;
import java.util.List;

public interface ProductSaveService {
    boolean productStatusUp(List<SkuEsModelTO> skuEsModelTOs) throws IOException;
}
