package com.indi.gulimall.search.vo;

import lombok.Data;

import java.util.List;

@Data
public class SearchParamVO {
    private String keyword; // 页面传递过来的全文匹配关键字

    /**
     *  过滤条件
     *
     *  category3Id=225
     *  brandId=1
     *  stockOrNot=0/1
     *  attrs=2_5寸:6寸   attrID = 2 attrValues = [5寸，6寸]
     *  skuPrice=1_500/_500/500_    1-500之间 小于500 大于500
     */
    private String category3Id; // 三级分类id
    private List<Long> brandId; // 按照品牌查询，可以多选
    private Integer stockOrNot;   // 是否只显示有货的商品
    private List<String> attrs;  // 按照属性进行筛选
    private String skuPrice;    // 价格区间查询
    private String queryString;   // 原生的所有查询条件

    /**
     * 排序条件
     *
     * sort=saleCount_asc/desc
     * sort=skuPrice_asc/desc
     * sort=hotScore_asc/desc
     */
    private String sort;    //

    private Integer pageNum = 1;    // 页码
}
