package com.indi.gulimall.search.vo;

import com.indi.common.to.es.SkuEsModelTO;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class SearchResultVO {
    private List<SkuEsModelTO> products;    // 查询到的商品信息

    private Integer pageNum;    // 当前页码
    private Long total; // 总记录数
    private Integer totalPages; // 总页码
    private List<Integer> pageNavs; // 总导航页码
    private List<BrandVO> brandVOs; // 查询到的品牌信息
    private List<CategoryVO> categoryVOs;   // 查询到的分类信息
    private List<AttrVO> attrVOs;   // 查询到的所有属性
    private List<NavVO> navVOs = new ArrayList<>(); // 面包屑导航的数据
    private List<Long> attrIds = new ArrayList<>();

    @Data
    public static class NavVO{
        private String navName;
        private String navValue;
        private String link;
    }

    @Data
    public static class BrandVO{
        private Long brandId;
        private String brandName;
        private String brandImg;
    }

    @Data
    public static class CategoryVO{
        private Long categoryId;
        private String categoryName;
    }

    @Data
    public static class AttrVO{
        private Long attrId;
        private String attrName;
        private List<String> attrValue;
    }
}
