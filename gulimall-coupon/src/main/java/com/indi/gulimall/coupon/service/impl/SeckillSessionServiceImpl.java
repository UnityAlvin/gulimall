package com.indi.gulimall.coupon.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.indi.common.utils.PageUtils;
import com.indi.common.utils.Query;
import com.indi.gulimall.coupon.dao.SeckillSessionDao;
import com.indi.gulimall.coupon.entity.SeckillSessionEntity;
import com.indi.gulimall.coupon.entity.SeckillSkuRelationEntity;
import com.indi.gulimall.coupon.service.SeckillSessionService;
import com.indi.gulimall.coupon.service.SeckillSkuRelationService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service("seckillSessionService")
public class SeckillSessionServiceImpl extends ServiceImpl<SeckillSessionDao, SeckillSessionEntity> implements SeckillSessionService {
    @Resource
    private SeckillSkuRelationService seckillSkuRelationService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SeckillSessionEntity> page = this.page(
                new Query<SeckillSessionEntity>().getPage(params),
                new QueryWrapper<SeckillSessionEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<SeckillSessionEntity> listByLatest3Days() {
        // 查出近3天的秒杀场次
        List<SeckillSessionEntity> seckillSessions = this.list(new QueryWrapper<SeckillSessionEntity>()
                .between("start_time", getStartTime(), getEndTime()));

        if (CollectionUtils.isNotEmpty(seckillSessions)) {
            // 查出每个场次的商品
            seckillSessions = seckillSessions.stream().map(item -> {
                List<SeckillSkuRelationEntity> seckillSkuRelations = seckillSkuRelationService
                        .listByPromotionSessionId(item.getId());
                item.setRelations(seckillSkuRelations);
                return item;
            }).collect(Collectors.toList());
        }

        return seckillSessions;
    }

    /**
     * 获取开始时间
     *
     * @return
     */
    private String getStartTime() {
        LocalDate today = LocalDate.now();  // 当前日期 年 月 日
        LocalTime minTime = LocalTime.MIN;  // 最小时间 00:00:00
        // 将日期和时间组合起来，并转换成想要的格式
        return LocalDateTime.of(today, minTime).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    }

    /**
     * 获取结束时间
     *
     * @return
     */
    private String getEndTime() {
        LocalDate twoDaysLaterDate = LocalDate.now().plusDays(2); // 两天之后的日期 年 月 日
        LocalTime maxTime = LocalTime.MAX;  // 最大时间 23:59:59

        // 将日期和时间组合起来，并转换成想要的格式
        return LocalDateTime.of(twoDaysLaterDate, maxTime).format(DateTimeFormatter
                .ofPattern("yyyy-MM-dd HH:mm:ss"));
    }
}