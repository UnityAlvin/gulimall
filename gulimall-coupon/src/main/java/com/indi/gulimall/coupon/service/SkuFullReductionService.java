package com.indi.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.indi.common.to.SkuReductionTO;
import com.indi.common.utils.PageUtils;
import com.indi.gulimall.coupon.entity.SkuFullReductionEntity;

import java.util.Map;

/**
 * 商品满减信息
 *
 * @author UnityAlvin
 * @email UnityAlvin@qq.com
 * @date 2021-05-17 09:30:16
 */
public interface SkuFullReductionService extends IService<SkuFullReductionEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveSkuReduction(SkuReductionTO skuReductionTO);
}

