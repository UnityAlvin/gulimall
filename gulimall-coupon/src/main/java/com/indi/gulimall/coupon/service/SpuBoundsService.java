package com.indi.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.indi.common.utils.PageUtils;
import com.indi.gulimall.coupon.entity.SpuBoundsEntity;

import java.util.Map;

/**
 * 商品spu积分设置
 *
 * @author UnityAlvin
 * @email UnityAlvin@qq.com
 * @date 2021-05-17 09:30:16
 */
public interface SpuBoundsService extends IService<SpuBoundsEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

