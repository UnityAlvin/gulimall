package com.indi.gulimall.coupon.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.indi.common.to.MemberPrice;
import com.indi.common.to.SkuReductionTO;
import com.indi.common.utils.PageUtils;
import com.indi.common.utils.Query;
import com.indi.gulimall.coupon.dao.SkuFullReductionDao;
import com.indi.gulimall.coupon.entity.MemberPriceEntity;
import com.indi.gulimall.coupon.entity.SkuFullReductionEntity;
import com.indi.gulimall.coupon.entity.SkuLadderEntity;
import com.indi.gulimall.coupon.mapstruct.CouponCovertBasic;
import com.indi.gulimall.coupon.service.MemberPriceService;
import com.indi.gulimall.coupon.service.SkuFullReductionService;
import com.indi.gulimall.coupon.service.SkuLadderService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service("skuFullReductionService")
public class SkuFullReductionServiceImpl extends ServiceImpl<SkuFullReductionDao, SkuFullReductionEntity> implements SkuFullReductionService {
    @Resource
    private SkuLadderService skuLadderService;

    @Resource
    private MemberPriceService memberPriceService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SkuFullReductionEntity> page = this.page(
                new Query<SkuFullReductionEntity>().getPage(params),
                new QueryWrapper<SkuFullReductionEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public void saveSkuReduction(SkuReductionTO skuReductionTO) {
        // 保存打折信息
        if (skuReductionTO.getFullCount() > 0) {
            SkuLadderEntity skuLadder = new SkuLadderEntity();
            skuLadder.setSkuId(skuReductionTO.getSkuId());
            skuLadder.setFullCount(skuReductionTO.getFullCount());
            skuLadder.setDiscount(skuReductionTO.getDiscount());
            skuLadder.setAddOther(skuReductionTO.getCountStatus());
            skuLadderService.save(skuLadder);
        }

        // 保存满减信息
        if (skuReductionTO.getFullPrice().compareTo(BigDecimal.ZERO) == 1) {
            SkuFullReductionEntity reduction = CouponCovertBasic.INSTANCE.skuReductionTOToSkuFullReduction(skuReductionTO);
            this.save(reduction);
        }

        // 保存会员信息
        List<MemberPrice> memberPriceTOS = skuReductionTO.getMemberPrice();
        List<MemberPriceEntity> memberPrices = memberPriceTOS.stream().map(memberPriceTO -> {
            MemberPriceEntity memberPrice = new MemberPriceEntity();
            memberPrice.setSkuId(skuReductionTO.getSkuId());
            memberPrice.setMemberLevelId(memberPriceTO.getId());
            memberPrice.setMemberLevelName(memberPriceTO.getName());
            memberPrice.setMemberPrice(memberPriceTO.getPrice());
            memberPrice.setAddOther(1);
            return memberPrice;
        }).filter(memberPrice -> memberPrice.getMemberPrice().compareTo(BigDecimal.ZERO) == 1).collect(Collectors.toList());
        memberPriceService.saveBatch(memberPrices);
    }

}