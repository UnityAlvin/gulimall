package com.indi.gulimall.coupon.mapstruct;

import com.indi.common.to.SkuReductionTO;
import com.indi.gulimall.coupon.entity.SkuFullReductionEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface CouponCovertBasic {
    CouponCovertBasic INSTANCE = Mappers.getMapper(CouponCovertBasic.class);

    SkuFullReductionEntity skuReductionTOToSkuFullReduction(SkuReductionTO skuReductionTO);
}
