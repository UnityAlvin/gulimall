# 架构
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210515181258966.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L0FsdmluMTk5NzY1,size_16,color_FFFFFF,t_70)

## 解析

客户通过任意客户端（app/Web）向服务器发送请求，

请求首先来到Nginx集群，Nginx将请求转交给Api网关（SpringCloud Gateway）,

Api网关：

1. 可以根据当前请求，动态路由到指定的服务 
2. 假如请求对应的服务过多，例如是查询商品，而商品在3个服务里面都有，网关可以通过Ribbon负载均衡的调用服务。
3. 如果服务出现问题，也可以在网关这个级别通过Sentinel做熔断降级
4. 还可以对请求进行认证授权， 请求过来以后，看是否合法，合法了以后再放行
5. 还可以通过Sentinel对请求进行限流，比如当前有100W个请求，我们害怕同时放进来之后，把后台服务压垮，可以在网关处进行限流控制，只放行1W个过去，让后台业务集群很容易的处理完这些请求。



SpringBoot：实现了微服务

Feign：实现了微服务之间的调用

OAuth2.0认证中心：实现了登录之后才能处理的请求、以及社交登录

Spring Securitiy：实现了整个应用的安全以及权限控制

缓存：使用Redis实现了分片集群以及哨兵集群

持久化：使用MySQL集群实现了读写分离、分库分表

消息队列：使用Rabbit MQ集群，实现微服务之间的异步解耦，包括完成分布式事务的最终一致性 

全文检索：使用Elastic Search实现

对象存储：使用阿里云的OOS存储服务



日志存储：使用ELK对日志进行相关处理，使用LogStash收集业务里面的各种日志，然后把它存储到ES里，然后ELK使用Kibana可视化插件从ES中检索出相关的日志信息，帮我们快速定位线上问题的所在。





Nacos注册中心：实现服务之间的注册与发现

Nacos配置中心：实现统一管理配置，实现改一处服务的配置，其它服务都自动修改，所有的服务都可以通过配置中心，动态获取它的配置

Sleuth+Zipkin：实现服务的可视化追踪 ，然后将所有追踪到的信息，交给Prometheus进行聚合分析，再有Grafana进行可视化展示，通过prometheus提供的Alertmanager可以实时的得到一些服务的告警信息，把这些告警信息以邮件或者短信的形式，通知开发以及运维人员





持续集成：开发人员将修改后的代码，提交到Github，运维人员可以通过自动化工具Jenkins Pipeline，从Github中获取到代码，将它打包成Docker镜像，最终使用K8S，集成整个Docker服务，我们将服务以Docker容器的方式去运行。



分布式事务：使用Seata


# 微服务划分
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210515181852292.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L0FsdmluMTk5NzY1,size_16,color_FFFFFF,t_70)

# 常用界面

## 商品服务

| 功能     | 地址                                 |
| :------- | :------------------------------------- |
| 首页 | http://gulimall.com/index.html    |
| 商品详情 | http://item.gulimall.com/{skuId}.html    |



## 检索服务

| 功能     | 地址                                   |
| :------- | :------------------------------------- |
| 查询商品 | http://search.gulimall.com/list.html    |



## 认证服务

| 功能 | 地址                                   |
| :--- | :------------------------------------- |
| 注册 | http://auth.gulimall.com/register.html |
| 登录 | http://auth.gulimall.com/login.html    |


## 购物车服务

| 功能地址   | 地址                               |
| :--------- | :--------------------------------- |
| 我的购物车 | http://cart.gulimall.com/cart.html |



## 订单服务

| 功能     | 地址                                   |
| :------- | :------------------------------------- |
| 我的订单 | http://order.gulimall.com/list.html    |
| 订单详情 | http://order.gulimall.com/detail.html  |
| 订单结算 | http://order.gulimall.com/confirm.html |
| 支付     | http://order.gulimall.com/pay.html    |

# 模块介绍

|                      | 服务           | 描述                                       |
| :-------------------- | :-------------- | :------------------------------------------ |
| gulimall-auth        | 认证服务       | 社交登录、Oauth2.0、单点登录               |
| gulimall-cart        | 购物车服务       | 购物车保存、合并、提交等               |
| gulimall-common      | 公共服务       | 常量、异常码、工具类、通用实体等           |
| gulimall-coupon      | 优惠服务       | 积分、优惠券等信息                         |
| gulimall-gateway     | 网关服务       | 网关接收前端请求做统一转发                 |
| gulimall-member      | 会员服务       | 会员基本信息、会员等级、会员积分等         |
| gulimall-order       | 订单服务       | 普通订单、秒杀订单等                       |
| gulimall-product     | 商品服务       | 商品规格、属性、品牌、分类等               |
| gulimall-search      | 检索服务       | Elasticsearch 检索                         |
| gulimall-seckill      | 秒杀服务       | 秒杀活动相关                         |
| gulimall-third-party | 第三方整合服务 | 阿里云OSS、阿里云短信                      |
| gulimall-ware        | 库存服务       | 采购工作单、库存等                         |
| renren-fast          | 人人后台       | 负责后台登录以及模块等信息                 |
| renren-generator     | 人人代码生成器 | 根据数据库快速生成后台的前端页面与后台代码 |

# 依赖版本

|                      | 版本          |
| :-------------------- | :------------- |
| Spring Boot          | 2.1.8.RELEASE |
| Spring Cloud         | Greenwich.SR3 |
| Spring Cloud Alibaba | 2.1.0.RELEASE |
| MySQL                | 8.0.17        |
| ElesticSearch        | 7.4.2         |
| MyBatisPlus          | 3.2.0         |
| Lombok               | 1.18.2        |
| Swagger              | 2.9.2         |
| Mapstruct            | 1.4.1.Final   |
| Redisson             | 3.12.0        |
| HttpComponents       | 4.4.12        |

# 服务器环境

|               | 版本号 | 描述                   |
| :------------- | :------ | :---------------------- |
| nginx         | 1.10   | 反向代理服务器         |
| elasticsearch | 7.4.2  | 搜索                   |
| kibana        | 7.4.2  | 可视化页面             |
| nacos         | 1.1.4  | 服务注册中心和配置中心 |
| redis         | ^      | 缓存                   |
| mysql         | 5.7    | 数据库                 |
| seata         | 0.7.1  | 分布式事务             |
| sentinel      | 1.6.3  | 监控平台               |
| zipkin        | ^      | 可视化的链路追踪       |