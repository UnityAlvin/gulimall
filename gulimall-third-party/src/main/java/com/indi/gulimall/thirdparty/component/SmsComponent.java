package com.indi.gulimall.thirdparty.component;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Data
@Component
@ConfigurationProperties(prefix = "spring.cloud.alicloud.sms")
public class SmsComponent {
    private String regionId;    // 所在区域
    private String keyId;   // keyid
    private String keySecret;   // keysecret
    private String templateCode;    // 你的短信模板code
    private String signName;    // 你的短信模板签名

    public void sendSmsCode(String phoneNum, String code) {
        // 创建远程连接客户端对象
        DefaultProfile profile = DefaultProfile.getProfile(regionId, keyId, keySecret);

        IAcsClient client = new DefaultAcsClient(profile);

        // 创建远程连接的请求参数
        CommonRequest request = new CommonRequest();
        request.setSysMethod(MethodType.POST);
        request.setSysDomain("dysmsapi.aliyuncs.com");
        request.setSysVersion("2017-05-25");
        request.setSysAction("SendSms");
        request.putQueryParameter("RegionId", regionId);
        request.putQueryParameter("SignName", signName);
        request.putQueryParameter("TemplateCode", templateCode);
        request.putQueryParameter("PhoneNumbers", phoneNum);
        HashMap<String, Object> map = new HashMap<>();
        map.put("code", code);
        String jsonString = JSON.toJSONString(map);
        request.putQueryParameter("TemplateParam", jsonString);

        try {
            // 使用客户端对象携带请求参数向远程阿里云服务器发起远程调用，并得到响应结果
            CommonResponse response = client.getCommonResponse(request);

            // 通信失败的处理
            boolean result = response.getHttpResponse().isSuccess();

            if (!result) {
                // ALIYUN_RESPONSE_FAIL(-501, "阿里云响应失败"),
            } else {
                String data = response.getData();
                HashMap<String, String> resultMap = JSON.parseObject(data, new TypeReference<HashMap<String, String>>() {
                });
                System.out.println("阿里云短信发送结果：" + "code：" + resultMap.get("Code") + "message:" + resultMap.get("Message"));
            }


        } catch (ClientException e) {
            e.printStackTrace();
        }

    }
}
