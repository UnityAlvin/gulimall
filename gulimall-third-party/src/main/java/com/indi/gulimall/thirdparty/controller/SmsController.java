package com.indi.gulimall.thirdparty.controller;

import com.indi.common.utils.R;
import com.indi.gulimall.thirdparty.component.SmsComponent;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/web/third-party")
public class SmsController {
    @Resource
    private SmsComponent smsComponent;

    @PostMapping("/sms/send-code")
    public R sendCode(@RequestParam("phoneNum") String phoneNum, @RequestParam("code") String code) {
        smsComponent.sendSmsCode(phoneNum,code);
        return R.ok();
    }
}
