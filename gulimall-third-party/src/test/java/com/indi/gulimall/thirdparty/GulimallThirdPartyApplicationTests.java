package com.indi.gulimall.thirdparty;

import com.indi.gulimall.thirdparty.component.SmsComponent;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GulimallThirdPartyApplicationTests {
    @Resource
    private SmsComponent smsComponent;

    @Test
    public void contextLoads() {
        smsComponent.sendSmsCode("11111111111","1234");
    }

}
