package com.indi.gulimall.order;

import com.indi.gulimall.order.entity.OrderReturnReasonEntity;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.Binding.DestinationType;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class GulimallOrderApplicationTests {

    @Autowired
    AmqpAdmin amqpAdmin;

    @Autowired
    RabbitTemplate rabbitTemplate;

    /**
     * 创建交换机
     */
    @Test
    public void createExchange() {
        /*
            名字
            是否持久化
            是否自动删除
         */
        DirectExchange directExchange = new DirectExchange("hello-java-exchange", true, false);
        amqpAdmin.declareExchange(directExchange);
        log.info("Exchange[{}]，创建成功", directExchange.getName());
    }

    /**
     * 创建队列
     */
    @Test
    public void createQueues() {
        /*
            名字
            是否持久化
            是否排他：指的是只要有一个人连上，就不允许其它人连接
            是否自动删除
         */
        Queue queue = new Queue("hello-java-queue", true, false, false);
        amqpAdmin.declareQueue(queue);
        log.info("Exchange[{}]，创建成功", queue.getName());
    }

    /**
     * 创建绑定
     */
    @Test
    public void createBinding() {
        /*
            将指定好目的地类型的目的地，与交换机进行绑定，并设置路由键

            destination：目的地
            destinationType：目的地类型
            exchange: 交换机
            routingKey: 路由键
            arguments: 自定义参数
         */
        Binding binding = new Binding("hello-java-queue", DestinationType.QUEUE, "hello-java-exchange", "hello.java", null);
        amqpAdmin.declareBinding(binding);
        log.info("绑定创建成功");
    }

    /**
     * 发送消息
     */
    @Test
    public void sendMessage() {
        String msg = "Hello World!";
        OrderReturnReasonEntity order = new OrderReturnReasonEntity();
        order.setId(1L);
        order.setName("qqq");

        /*
            交换机名称
            路由键
            消息内容
         */
        // 1、发送文本
//        rabbitTemplate.convertAndSend("hello-java-exchange", "hello.java", msg);

        // 2、发送对象
        // 如果发送的消息是个对象，则这个对象必须实现Serializable
        rabbitTemplate.convertAndSend("hello-java-exchange", "hello.java", order);

        log.info("消息发送成功");
    }

}
