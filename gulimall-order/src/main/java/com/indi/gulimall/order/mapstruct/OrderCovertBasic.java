package com.indi.gulimall.order.mapstruct;

import com.indi.common.dto.OrderDTO;
import com.indi.common.dto.WareLockStockDTO.OrderItemDTO;
import com.indi.gulimall.order.dto.MemberReceiveAddressDTO;
import com.indi.gulimall.order.entity.OrderEntity;
import com.indi.gulimall.order.entity.OrderItemEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

/**
 * @author UnityAlvin
 * @date 2021/7/18 15:16
 */
@Mapper(componentModel = "spring")
public interface OrderCovertBasic {
    OrderCovertBasic INSTANCE = Mappers.getMapper(OrderCovertBasic.class);

    @Mappings({
            @Mapping(source = "name", target = "receiverName"),
            @Mapping(source = "phone", target = "receiverPhone"),
            @Mapping(source = "postCode", target = "receiverPostCode"),
            @Mapping(source = "province", target = "receiverProvince"),
            @Mapping(source = "city", target = "receiverCity"),
            @Mapping(source = "region", target = "receiverRegion"),
            @Mapping(source = "detailAddress", target = "receiverDetailAddress"),
    })
    OrderEntity MemberReceiveAddressDTOToOrder(MemberReceiveAddressDTO memberReceiveAddressDTO);

    OrderItemDTO orderItemToWareLockStockDTO(OrderItemEntity orderItem);

    OrderDTO orderToOrderDTO(OrderEntity currentOrder);
}
