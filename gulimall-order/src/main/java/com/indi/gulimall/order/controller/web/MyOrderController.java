package com.indi.gulimall.order.controller.web;

import com.indi.common.utils.PageUtils;
import com.indi.gulimall.order.service.OrderService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * 我的订单
 *
 * @author UnityAlvin
 * @date 2021/7/25 21:44
 */
@Controller
public class MyOrderController {
    @Resource
    private OrderService orderService;

    @GetMapping("/my-order.html")
    public String orderList(@RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                            @RequestParam(value = "pageNum", defaultValue = "5") Integer limit,
                            Model model) {
        Map<String ,Object> pages = new HashMap<>();
        pages.put("page",pageNum);
        pages.put("limit",limit);
        PageUtils orderPages = orderService.queryOrderPage(pages);
        model.addAttribute("orderPages",orderPages);
        return "my-order";
    }
}
