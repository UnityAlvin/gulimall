package com.indi.gulimall.order.controller.web;

import com.indi.common.dto.mq.SeckillSkuDTO;
import com.indi.common.exception.BusinessException;
import com.indi.common.utils.R;
import com.indi.gulimall.order.entity.OrderEntity;
import com.indi.gulimall.order.service.OrderService;
import com.indi.gulimall.order.vo.*;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.concurrent.ExecutionException;

@Controller
public class OrderController {
    @Resource
    private OrderService orderService;

    //    @GetMapping("/{page}.html")
//    public String testPage(@PathVariable("page") String page) {
//        return page;
//    }
    @Resource
    private RabbitTemplate rabbitTemplate;

    /**
     * 跳转订单确认页面
     *
     * @param model
     * @return
     * @throws ExecutionException
     * @throws InterruptedException
     */
    @GetMapping("/web/order/to-trade")
    public String toTrade(Model model) throws ExecutionException, InterruptedException {
        OrderConfirmVO orderConfirmVO = orderService.getConfirmInfo();
        model.addAttribute("orderConfirmData", orderConfirmVO);
        return "confirm";
    }

    /**
     * 提交订单
     *
     * @param orderSubmitVO
     * @param model
     * @param redirectAttributes
     * @return
     */
    @PostMapping("/web/order/submit-order")
    public String submitOrder(OrderSubmitVO orderSubmitVO,
                              Model model, RedirectAttributes redirectAttributes) {
        StringBuilder msg = new StringBuilder();

        try {
            OrderSubmitResponseVO respVO = orderService.submitOrder(orderSubmitVO);
            switch (respVO.getCode()) {
                case 0:
                    model.addAttribute("submitOrderResp", respVO);
                    return "pay";
                case 1:
                    msg.append("页面过期，请返回购物车重新提交");
                case 2:
                    msg.append("商品价格发生变化，请确认后再次提交");
                default:
                    redirectAttributes.addAttribute("msg", msg.toString());
                    return "redirect:http://order.gulimall.com/web/order/to-trade";
            }
        } catch (BusinessException e) {
            // 没办法使用addFlashAttribute，找不到解决办法
            redirectAttributes.addAttribute("msg", e.getMessage());
            return "redirect:http://order.gulimall.com/web/order/to-trade";
        }

    }

    /**
     * 通过订单编号查询订单信息
     *
     * @param orderSn
     * @return
     */
    @GetMapping("/web/order/order-by-order-sn/{orderSn}")
    @ResponseBody
    public R infoByOrderSn(@PathVariable("orderSn") String orderSn) {
        OrderEntity order = orderService.getOrderByOrderSn(orderSn);
        return R.ok().setData(order);
    }

    /**
     * 获取秒杀订单数据
     */
    @GetMapping("/web/order/seckill-order-info")
    public String getSeckillOrderInfo(@RequestParam("promotionSessionId") Long promotionSessionId,
                                      @RequestParam("skuId") Long skuId,
                                      @RequestParam("seckillPrice") BigDecimal seckillPrice,
                                      @RequestParam("num") Integer num,
                                      @RequestParam("orderSn") String  orderSn,
                                      Model model) throws ExecutionException, InterruptedException {
        SeckillSkuDTO seckillSkuDTO = new SeckillSkuDTO();
        seckillSkuDTO.setPromotionSessionId(promotionSessionId);
        seckillSkuDTO.setSkuId(skuId);
        seckillSkuDTO.setPrice(seckillPrice);
        seckillSkuDTO.setNum(num);
        seckillSkuDTO.setOrderSn(orderSn);
        SeckillOrderConfirmVO seckillOrderConfirmVO = orderService.getSeckillOrderInfo(seckillSkuDTO);
        model.addAttribute("seckillOrder", seckillOrderConfirmVO);
        return "seckill-order-confirm";
    }

    /**
     * 提交秒杀订单
     *
     * @param seckillOrderSubmitVO
     * @return
     */
    @PostMapping("/web/order/submit-seckill-order")
    public String submitSeckillOrder(SeckillOrderSubmitVO seckillOrderSubmitVO,
                                     Model model) {
        OrderSubmitResponseVO respVO = orderService.submitSeckillOrder(seckillOrderSubmitVO);
        model.addAttribute("submitOrderResp", respVO);
        return "pay";
    }
}
