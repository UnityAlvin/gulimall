package com.indi.gulimall.order.dao;

import com.indi.gulimall.order.entity.OrderItemEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单项信息
 * 
 * @author UnityAlvin
 * @email UnityAlvin@qq.com
 * @date 2021-05-17 09:15:18
 */
@Mapper
public interface OrderItemDao extends BaseMapper<OrderItemEntity> {
	
}
