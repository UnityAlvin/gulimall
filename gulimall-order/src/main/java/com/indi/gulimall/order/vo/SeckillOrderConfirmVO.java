package com.indi.gulimall.order.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * 秒杀订单页面所需要的信息
 */
@Data
public class SeckillOrderConfirmVO {
    private String orderSn;
    private List<MemberAddressVO> address;
    private Integer integration;       // 积分
    private BigDecimal totalPrice;   // 订单总额
    private BigDecimal payPrice;    // 应付价格
    private String orderToken;      // 防重令牌：防止用户重复提交订单
    private BigDecimal freight;  // 运费
    private String title;
    private String image;
    private List<String> skuAttr;
    private BigDecimal price;   // 秒杀单价
    private Integer num;    // 秒杀数量
    private Long promotionSessionId;
    private Long skuId;
    private String randomCode;

    /**
     * 会员地址信息
     */
    @Data
    public static class MemberAddressVO {
        private Long id;
        private Long memberId;          // member_id
        private String name;            // 收货人姓名
        private String phone;           // 电话
        private String postCode;        // 邮政编码
        private String province;        // 省份/直辖市
        private String city;            // 城市
        private String region;          // 区
        private String detailAddress;   // 详细地址(街道)
        private String areacode;        // 省市区代码
        private Integer defaultStatus;  // 是否默认
    }
}
