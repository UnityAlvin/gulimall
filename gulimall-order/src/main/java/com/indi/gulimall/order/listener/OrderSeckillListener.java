package com.indi.gulimall.order.listener;

import com.indi.common.constant.OrderConstant;
import com.indi.common.dto.mq.SeckillSkuDTO;
import com.indi.gulimall.order.service.OrderService;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;

/**
 *
 * @author UnityAlvin
 * @date 2021/7/29 15:34
 */
@RabbitListener(queues = OrderConstant.ORDER_SECKILL_ORDER_QUEUE)
@Service
public class OrderSeckillListener {
    @Resource
    private OrderService orderService;

    @RabbitHandler
    private void handleSeckillOrder(SeckillSkuDTO seckillSkuDTO, Message message, Channel channel) throws IOException {
        try {
            System.out.println("收到消息，创建秒杀订单");
            orderService.createSeckillOrder(seckillSkuDTO);
            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
        } catch (Exception e) {
            e.printStackTrace();
            channel.basicReject(message.getMessageProperties().getDeliveryTag(), true);
        }
    }
}
