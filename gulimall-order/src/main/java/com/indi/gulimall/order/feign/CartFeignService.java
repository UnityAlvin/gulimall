package com.indi.gulimall.order.feign;

import com.indi.gulimall.order.vo.OrderConfirmVO.OrderItemVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

/**
 * @author UnityAlvin
 * @date   2021/7/16 23:30
 */
@FeignClient("gulimall-cart")
public interface CartFeignService {
    /**
     * 获取当前用户购物车中所有选中的购物项
     * @return
     */
    @GetMapping("/web/cart/current-user-cart-items")
    List<OrderItemVO> getCurrentUserCartItems();
}
