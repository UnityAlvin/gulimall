package com.indi.gulimall.order.dto;

import com.indi.gulimall.order.entity.OrderEntity;
import com.indi.gulimall.order.entity.OrderItemEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * 一个完整订单的所有数据
 * @author UnityAlvin
 * @date 2021/7/18 15:28
 */
@Data
public class OrderCreateDTO {
    private OrderEntity order;
    private List<OrderItemEntity> orderItems;
    private BigDecimal payPrice;
}
