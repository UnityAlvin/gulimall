package com.indi.gulimall.order.feign;

import com.indi.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author UnityAlvin
 * @date 2021/7/18 19:21
 */
@FeignClient("gulimall-product")
public interface ProductFeignService {
    @GetMapping("/web/product/spu-info/info/{skuId}")
    R info(@PathVariable("skuId") Long skuId);

    /**
     * 根据skuId获取拼接后的值
     * 格式 -> 销售属性：属性值
     * @param skuId
     * @return
     */
    @GetMapping("/web/product/sku-sale-attr-value/get-attr-name-and-values/{skuId}")
    R getSaleAttrNameAndValues(@PathVariable("skuId") Long skuId);
}
