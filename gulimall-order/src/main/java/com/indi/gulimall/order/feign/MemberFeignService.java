package com.indi.gulimall.order.feign;

import com.indi.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author UnityAlvin
 * @date   2021/7/16 23:31
 */
@FeignClient("gulimall-member")
public interface MemberFeignService {
    /**
     * 根据会员id获取地址信息
     * @param memberId
     * @return
     */
    @GetMapping("/web/member/member-receive-address/address/{memberId}")
    R getAddress(@PathVariable("memberId") Long memberId);

    /**
     * 根据地址id查询地址信息
     * @param id
     * @return
     */
    @GetMapping("/web/member/member-receive-address/info/{id}")
    R getReceiveAddressInfo(@PathVariable("id") Long id);
}
