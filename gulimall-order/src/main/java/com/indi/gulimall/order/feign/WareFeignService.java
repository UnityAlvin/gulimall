package com.indi.gulimall.order.feign;

import com.indi.common.dto.WareLockStockDTO;
import com.indi.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author UnityAlvin
 * @date 2021/7/17 15:48
 */
@FeignClient("gulimall-ware")
public interface WareFeignService {
    @PostMapping("/admin/ware/ware-sku/stock-or-not")
    R stockOrNot(@RequestBody List<Long> skuIds);

    /**
     * 为下单商品锁定库存
     * @param wareLockStockDTO
     * @return
     */
    @PostMapping("/web/ware/ware-sku/lockStock")
    R lockStock(@RequestBody WareLockStockDTO wareLockStockDTO);
}
