package com.indi.gulimall.order.controller.web;

import com.alipay.api.AlipayApiException;
import com.indi.gulimall.order.config.AlipayTemplate;
import com.indi.gulimall.order.service.OrderService;
import com.indi.gulimall.order.vo.PayVO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * @author UnityAlvin
 * @date 2021/7/25 18:41
 */
@Controller
public class OrderPayController {
    @Resource
    private AlipayTemplate alipayTemplate;

    @Resource
    private OrderService orderService;

    /**
     * 支付宝支付
     *
     * @param orderSn
     * @return
     */
    @GetMapping(value = "/web/order-pay/alipay", produces = "text/html")
    @ResponseBody
    public String alipay(@RequestParam String orderSn) throws AlipayApiException {
        PayVO payVO = orderService.getOrderPayInfo(orderSn);
        String routePage = alipayTemplate.pay(payVO);
        return routePage;
    }
}
