package com.indi.gulimall.order.controller;

import com.indi.gulimall.order.entity.OrderEntity;
import com.indi.gulimall.order.entity.OrderReturnReasonEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * 为了精准使用controller测试Rabbit发送多条消息
 */
@RestController
@Slf4j
public class RabbitController {
    @Autowired
    RabbitTemplate rabbitTemplate;

    /**
     * 测试创建订单，一分钟以后自动关单
     * @return
     */
    @GetMapping("/test/create-order")
    @ResponseBody
    public String createOrder(){
        OrderEntity order = new OrderEntity();
        order.setOrderSn(UUID.randomUUID().toString());
        rabbitTemplate.convertAndSend("order-event-exchange","order.create.order",order);
        return "ok";
    }

    /**
     * 批量发送消息
     */
    @GetMapping("/sendMultiMsg")
    public String sendMoreMessage() {
        for (int i = 0; i < 10; i++) {
            String msg = "Hello World!";
            OrderReturnReasonEntity order = new OrderReturnReasonEntity();
            order.setId(Long.parseLong(i + ""));
            order.setName("qqq");

            // 交换机名称 | 路由键 | 消息内容
            // 1、发送出文本
//        rabbitTemplate.convertAndSend("hello-java-exchange", "hello.java", msg);

            // 2、发送对象
            // 如果发送的消息是个对象，则这个对象必须实现Serializable
            rabbitTemplate.convertAndSend("hello-java-exchange", "hello.java", order);

            log.info("消息发送成功" + i);
        }
        return "ok";
    }

    /**
     * 发送不同类型的消息
     *
     * @param num
     */
    @GetMapping("/sendDiffMsg")
    public String sendDiffMsg(@RequestParam(value = "num", defaultValue = "10") Integer num) {
        for (int i = 0; i < num; i++) {
            String msg = "Hello World!";
            OrderReturnReasonEntity reason = new OrderReturnReasonEntity();
            reason.setId(Long.parseLong(i + ""));
            reason.setName("qqq");
            if (i % 2 == 0) {
                // 发送OrderReturnReasonEntity类型的消息
//                rabbitTemplate.convertAndSend("hello-java-exchange", "hello.java", reason);

                /*
                    发送指定了唯一id的消息

                    交换机名称
                    路由键
                    消息内容
                    消息的唯一id：
                        除了将其发送给MQ，还可以将其保存到MySQL数据库里面，相当于每一个唯一id对应一个消息
                        如果服务端收到了这个消息，那就在数据库里面说一下这个数据已经收到了，如果想要知道哪
                        些消息没收到，只需定时扫描数据库，查看哪条消息是没收到状态，然后重新发送即可
                 */
                rabbitTemplate.convertAndSend("hello-java-exchange", "hello.java", reason,
                        new CorrelationData(UUID.randomUUID().toString()));

            } else {
                OrderEntity order = new OrderEntity();
                order.setBillContent(UUID.randomUUID().toString());
                // 发送OrderEntity类型的消息
//                rabbitTemplate.convertAndSend("hello-java-exchange", "hello.java", order);

                // 发送错误消息
//                rabbitTemplate.convertAndSend("hello-java-exchange", "hello2.java", order);

                // 发送指定了唯一id的消息
                rabbitTemplate.convertAndSend("hello-java-exchange", "hello.java", order,
                        new CorrelationData(UUID.randomUUID().toString()));

            }
            log.info("消息发送成功" + i);
        }
        return "ok";
    }
}
