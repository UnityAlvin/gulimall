package com.indi.gulimall.cart.service;

import com.indi.gulimall.cart.vo.CartVO;
import com.indi.gulimall.cart.vo.CartVO.CartItemVO;

import java.util.List;
import java.util.concurrent.ExecutionException;

public interface CartService {
    /**
     * 添加商品到购物车中
     * @param skuId
     * @param num
     * @return
     */
    void addToCart(Long skuId, Integer num) throws ExecutionException, InterruptedException;

    /**
     * 根据skuId获取购物车中的购物项
     * @param skuId
     * @return
     */
    CartItemVO getCartItem(Long skuId);

    /**
     * 获取整个购物车
     * @return
     */
    CartVO getCart() throws ExecutionException, InterruptedException;

    /**
     * 清空购物车
     * @param cartKey
     */
    void clearCart(String cartKey);

    /**
     * 选中/不选中购物项
     * @param skuId
     * @param check
     * @return
     */
    void checkItem(Long skuId, Integer check);

    /**
     * 修改购物项数量
     * @param skuId
     * @param num
     */
    void changeItemCount(Long skuId, Integer num);

    /**
     * 删除购物项
     * @param skuId
     */
    void deleteItem(Long skuId);

    /**
     * 获取当前用户的所有选中购物项
     * @return
     */
    List<CartItemVO> getCurrentUserCartItems();
}
