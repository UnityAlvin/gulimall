package com.indi.gulimall.cart.controller;

import com.indi.gulimall.cart.service.CartService;
import com.indi.gulimall.cart.vo.CartVO;
import com.indi.gulimall.cart.vo.CartVO.CartItemVO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Controller
public class CartController {
    @Resource
    private CartService cartService;

    /**
     * 添加商品到购物车
     *
     * @param skuId
     * @param num
     * @param ra
     * @return
     * @throws ExecutionException
     * @throws InterruptedException
     */
    @GetMapping("/web/cart/add-to-cart")
    public String addToCart(@RequestParam("skuId") Long skuId,
                            @RequestParam("num") Integer num,
                            RedirectAttributes ra) throws ExecutionException, InterruptedException {
        cartService.addToCart(skuId, num);
        // RedirectAttributes.addAttribute()可以自动给重定向地址拼上参数
        ra.addAttribute("skuId", skuId);
        return "redirect:http://cart.gulimall.com/add-to-cart-success.html";
    }

    /**
     * 跳转到成功页
     * 使用重定向的方式，解决重复刷新页面，导致购物车数据无限提交
     * @param skuId
     * @param model
     * @return
     */
    @GetMapping("/add-to-cart-success.html")
    public String addToCartSuccessPage(@RequestParam("skuId") Long skuId, Model model) {
        // 为了解决重定向之后的数据丢失，需要重新查询购物车数据
        CartItemVO cartItemVO = cartService.getCartItem(skuId);
        model.addAttribute("item", cartItemVO);
        return "success";
    }

    /**
     * 我的购物车
     * @param model
     * @return
     * @throws ExecutionException
     * @throws InterruptedException
     */
    @GetMapping("/cart.html")
    public String cartListPage(Model model) throws ExecutionException, InterruptedException {
        CartVO cartVO = cartService.getCart();
        model.addAttribute("cart",cartVO);
        // 从ThreadLocal中快速得到用户信息
        return "cart-list";
    }

    /**
     * 选中/不选中购物项
     * @param skuId
     * @param check
     * @return
     */
    @GetMapping("/web/cart/check-item")
    public String checkItem(@RequestParam("skuId") Long skuId,
                            @RequestParam("check") Integer check,
                            Model model){
        cartService.checkItem(skuId,check);
        // 重定向到我的购物车，重新获取页面所需的数据
        return "redirect:http://cart.gulimall.com/cart.html";
    }

    /**
     * 增/减购物项的商品数量
     * @param skuId
     * @param num
     * @return
     */
    @GetMapping("/web/cart/count-item")
    public String countItem(@RequestParam("skuId") Long skuId,
                            @RequestParam("num") Integer num){
        cartService.changeItemCount(skuId,num);
        return "redirect:http://cart.gulimall.com/cart.html";
    }

    /**
     * 删除购物项
     * @param skuId
     * @return
     */
    @GetMapping("/web/cart/delete-item")
    public String deleteItem(@RequestParam("skuId") Long skuId){
        cartService.deleteItem(skuId);
        return "redirect:http://cart.gulimall.com/cart.html";
    }

    /**
     * 获取当前用户购物车中所有选中的购物项
     * @return
     */
    @GetMapping("/web/cart/current-user-cart-items")
    @ResponseBody
    public List<CartItemVO> getCurrentUserCartItems(){
        List<CartItemVO> cartItemVOs = cartService.getCurrentUserCartItems();
        return cartItemVOs;
    }
}
