package com.indi.gulimall.cart.feign;

import com.indi.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient("gulimall-product")
public interface ProductFeignService {
    /**
     * 根据skuId查询商品信息
     * @param skuId
     * @return
     */
    @GetMapping("/admin/product/sku-info/info/{skuId}")
    R info(@PathVariable("skuId") Long skuId);

    /**
     * 根据skuId获取拼接后的值
     * 格式 -> 销售属性：属性值
     * @param skuId
     * @return
     */
    @GetMapping("/web/product/sku-sale-attr-value/get-attr-name-and-values/{skuId}")
    R getSaleAttrNameAndValues(@PathVariable("skuId") Long skuId);

}
