package com.indi.gulimall.cart.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;


@Data
public class CartVO {
    private List<CartItemVO> items; // 所有购物车的商品
    private Integer countNum;   // 所有商品的总数量
    private Integer countType;  // 多少种商品
    private BigDecimal totalAmount; // 商品总价
    private BigDecimal reduce = BigDecimal.ZERO;  // 减免价格

    @Data
    public static class CartItemVO {
        private Long skuId;
        private Boolean check = true;
        private String title;
        private String image;
        private List<String> skuAttr;
        private BigDecimal price;
        private Integer count;
        private BigDecimal totalPrice;
    }
}
