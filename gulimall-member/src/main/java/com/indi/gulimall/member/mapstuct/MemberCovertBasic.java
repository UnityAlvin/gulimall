package com.indi.gulimall.member.mapstuct;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface MemberCovertBasic {
    MemberCovertBasic INSTANCE = Mappers.getMapper(MemberCovertBasic.class);
}
