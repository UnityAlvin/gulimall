package com.indi.gulimall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.indi.common.utils.PageUtils;
import com.indi.gulimall.member.entity.IntegrationChangeHistoryEntity;

import java.util.Map;

/**
 * 积分变化历史记录
 *
 * @author UnityAlvin
 * @email UnityAlvin@qq.com
 * @date 2021-05-17 09:34:28
 */
public interface IntegrationChangeHistoryService extends IService<IntegrationChangeHistoryEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

