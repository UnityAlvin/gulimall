package com.indi.gulimall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.indi.common.to.LoginTO;
import com.indi.common.to.RegisterTO;
import com.indi.common.utils.PageUtils;
import com.indi.gulimall.member.entity.MemberEntity;

import java.util.Map;

/**
 * 会员
 *
 * @author UnityAlvin
 * @email UnityAlvin@qq.com
 * @date 2021-05-17 09:34:28
 */
public interface MemberService extends IService<MemberEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void register(RegisterTO registerTO);

    void checkUsernameUnique(String userName);

    void checkPhoneNumUnique(String phoneNum);

    MemberEntity login(LoginTO loginTO);

    MemberEntity giteeLogin(String giteeInfo) throws Exception;
}

