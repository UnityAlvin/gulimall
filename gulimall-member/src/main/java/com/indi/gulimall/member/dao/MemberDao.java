package com.indi.gulimall.member.dao;

import com.indi.gulimall.member.entity.MemberEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员
 * 
 * @author UnityAlvin
 * @email UnityAlvin@qq.com
 * @date 2021-05-17 09:34:28
 */
@Mapper
public interface MemberDao extends BaseMapper<MemberEntity> {
	
}
