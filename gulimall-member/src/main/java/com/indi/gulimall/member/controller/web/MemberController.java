package com.indi.gulimall.member.controller.web;

import com.indi.common.to.LoginTO;
import com.indi.common.to.RegisterTO;
import com.indi.common.utils.R;
import com.indi.gulimall.member.entity.MemberEntity;
import com.indi.gulimall.member.service.MemberService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/web/member/member")
public class MemberController {
    @Resource
    private MemberService memberService;

    @PostMapping("/register")
    public R register(@RequestBody RegisterTO registerTO) {
        memberService.register(registerTO);
        return R.ok();
    }

    @PostMapping("/login")
    public R login(@RequestBody LoginTO loginTO){
        MemberEntity member = memberService.login(loginTO);
        return R.ok().setData(member);
    }

    @PostMapping("/gitee-login")
    public R giteeLogin(@RequestParam("giteeInfo") String giteeInfo) throws Exception {
        MemberEntity member = memberService.giteeLogin(giteeInfo);
        return R.ok().setData(member);
    }

}
