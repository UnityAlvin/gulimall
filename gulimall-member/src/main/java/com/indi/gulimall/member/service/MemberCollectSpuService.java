package com.indi.gulimall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.indi.common.utils.PageUtils;
import com.indi.gulimall.member.entity.MemberCollectSpuEntity;

import java.util.Map;

/**
 * 会员收藏的商品
 *
 * @author UnityAlvin
 * @email UnityAlvin@qq.com
 * @date 2021-05-17 09:34:28
 */
public interface MemberCollectSpuService extends IService<MemberCollectSpuEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

