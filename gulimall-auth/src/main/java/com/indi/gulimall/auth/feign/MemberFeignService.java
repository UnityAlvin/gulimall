package com.indi.gulimall.auth.feign;

import com.indi.common.to.LoginTO;
import com.indi.common.to.RegisterTO;
import com.indi.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("gulimall-member")
public interface MemberFeignService {
    @PostMapping("/web/member/member/register")
    R register(@RequestBody RegisterTO registerTO);

    @PostMapping("/web/member/member/login")
    R login(@RequestBody LoginTO loginTO);

    @PostMapping("/web/member/member/gitee-login")
    R giteeLogin(@RequestParam("giteeInfo") String giteeInfo);
}
