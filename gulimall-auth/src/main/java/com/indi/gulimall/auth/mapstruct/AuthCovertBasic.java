package com.indi.gulimall.auth.mapstruct;

import com.indi.common.to.LoginTO;
import com.indi.common.to.RegisterTO;
import com.indi.gulimall.auth.vo.LoginVO;
import com.indi.gulimall.auth.vo.RegisterVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface AuthCovertBasic {
    AuthCovertBasic INSTANCE = Mappers.getMapper(AuthCovertBasic.class);

    RegisterTO registerVOToRegisterTO(RegisterVO registerVO);

    LoginTO loginVOToLoginTO(LoginVO loginVO);
}