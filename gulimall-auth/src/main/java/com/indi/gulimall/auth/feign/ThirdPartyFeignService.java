package com.indi.gulimall.auth.feign;

import com.indi.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("gulimall-third-party")
public interface ThirdPartyFeignService {
    @PostMapping("/web/third-party/sms/send-code")
    R sendCode(@RequestParam("phoneNum") String phoneNum, @RequestParam("code") String code);
}
