package com.indi.gulimall.product;

import com.indi.gulimall.product.dao.ProductAttrValueDao;
import com.indi.gulimall.product.dao.SkuSaleAttrValueDao;
import com.indi.gulimall.product.entity.BrandEntity;
import com.indi.gulimall.product.service.BrandService;
import com.indi.gulimall.product.vo.web.SkuItemVO;
import com.indi.gulimall.product.vo.web.SkuItemVO.SaleAttrVO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GulimallProductApplicationTests {
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    BrandService brandService;

    @Resource
    private RedissonClient redissonClient;

    @Resource
    private ProductAttrValueDao attrValueDao;

    @Resource
    private SkuSaleAttrValueDao saleAttrValueDao;

    @Test
    public void testRedisson() {
        System.out.println(redissonClient);
    }

    @Test
    public void testRedis() {
        ValueOperations<String, String> ops = stringRedisTemplate.opsForValue();

        // 存值
        ops.set("hello", "world_" + UUID.randomUUID());

        // 取值
        System.out.println(ops.get("hello"));
    }

    @Test
    public void contextLoads() {
        BrandEntity brandEntity = new BrandEntity();
        brandEntity.setName("华为");
        brandService.save(brandEntity);
    }

    @Test
    public void testGetAttrValueWithGroup() {
        List<SkuItemVO.SpuAttrGroupVO> attrGroupVOs = attrValueDao.getAttrWithGroupVO(225L, 1L);
        attrGroupVOs.forEach(System.out::println);
    }

    @Test
    public void testGetSaleAttrValues(){
        List<SaleAttrVO> saleAttrVOs = saleAttrValueDao.getSaleAttrValues(4L);
        saleAttrVOs.forEach(System.out::println);
    }
}
