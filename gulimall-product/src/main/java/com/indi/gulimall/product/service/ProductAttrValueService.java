package com.indi.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.indi.common.utils.PageUtils;
import com.indi.gulimall.product.entity.ProductAttrValueEntity;
import com.indi.gulimall.product.vo.web.SkuItemVO;

import java.util.List;
import java.util.Map;

/**
 * spu属性值
 *
 * @author UnityAlvin
 * @email UnityAlvin@qq.com
 * @date 2021-05-17 09:49:38
 */
public interface ProductAttrValueService extends IService<ProductAttrValueEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<ProductAttrValueEntity> listBySpuId(Long spuId);

    void updateBySpuId(Long spuId, List<ProductAttrValueEntity> attrValues);

    List<SkuItemVO.SpuAttrGroupVO> getAttrWithGroupVO(Long categoryId, Long spuId);
}

