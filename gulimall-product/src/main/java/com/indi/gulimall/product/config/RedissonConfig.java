package com.indi.gulimall.product.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RedissonConfig {
    /**
     * 所有对Redisson的使用都是通过RedissonClient对象
     * @return
     */
    @Bean(destroyMethod = "shutdown")   // 服务停止以后，会调用shutdown进行销毁
    RedissonClient redission() {
        Config config = new Config();
        config.useSingleServer().setAddress("redis://192.168.2.190:6379");
        return Redisson.create(config);
    }
}
