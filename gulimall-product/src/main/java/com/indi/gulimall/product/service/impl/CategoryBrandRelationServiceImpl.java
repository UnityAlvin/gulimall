package com.indi.gulimall.product.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.indi.common.utils.PageUtils;
import com.indi.common.utils.Query;
import com.indi.gulimall.product.dao.CategoryBrandRelationDao;
import com.indi.gulimall.product.entity.BrandEntity;
import com.indi.gulimall.product.entity.CategoryBrandRelationEntity;
import com.indi.gulimall.product.entity.CategoryEntity;
import com.indi.gulimall.product.service.BrandService;
import com.indi.gulimall.product.service.CategoryBrandRelationService;
import com.indi.gulimall.product.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service("categoryBrandRelationService")
public class CategoryBrandRelationServiceImpl extends ServiceImpl<CategoryBrandRelationDao, CategoryBrandRelationEntity> implements CategoryBrandRelationService {

    @Autowired
    private BrandService brandService;

    @Autowired
    private CategoryService categoryService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryBrandRelationEntity> page = this.page(
                new Query<CategoryBrandRelationEntity>().getPage(params),
                new QueryWrapper<CategoryBrandRelationEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public void saveDetail(CategoryBrandRelationEntity relation) {
        Long brandId = relation.getBrandId();
        Long categoryId = relation.getCategoryId();

        // 1、查询详细名字
        BrandEntity brand = brandService.getBaseMapper().selectById(brandId);
        CategoryEntity category = categoryService.getBaseMapper().selectById(categoryId);

        relation.setBrandName(brand.getName());
        relation.setCategoryName(category.getName());
        this.save(relation);
    }

    @Override
    public void updateBrand(Long brandId, String name) {
        CategoryBrandRelationEntity relation = new CategoryBrandRelationEntity();
        relation.setBrandId(brandId);
        relation.setBrandName(name);

        UpdateWrapper<CategoryBrandRelationEntity> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("brand_id", brandId);
        this.update(relation, updateWrapper);
    }

    @Override
    public void updateCategory(Long catId, String name) {
        CategoryBrandRelationEntity relation = new CategoryBrandRelationEntity();
        relation.setCategoryId(catId);
        relation.setCategoryName(name);

        UpdateWrapper<CategoryBrandRelationEntity> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("category_id", catId);
        this.update(relation, updateWrapper);
    }

    @Override
    public List<BrandEntity> getBrandList(Long categoryId) {
        List<CategoryBrandRelationEntity> relationList = this.list(new QueryWrapper<CategoryBrandRelationEntity>()
                .eq("category_id", categoryId));

        if (relationList != null && relationList.size() > 0) {
            List<BrandEntity> brandList = relationList.stream().map(item -> {
                BrandEntity brand = brandService.getById(item.getBrandId());
                return brand;
            }).collect(Collectors.toList());
            return brandList;
        }
        return null;
    }

}