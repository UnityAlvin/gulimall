package com.indi.gulimall.product.vo;

import com.indi.gulimall.product.entity.AttrEntity;
import lombok.Data;

import java.util.List;

@Data
public class AttrGroupWithAttrsVO {
    private Long attrGroupId;
    private String attrGroupName;
    private Integer sort;
    private String descript;
    private String icon;
    private Long categoryId;
    private List<AttrEntity> attrs;
}
