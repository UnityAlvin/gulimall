package com.indi.gulimall.product.vo.spusave;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Bounds {
  private BigDecimal buyBounds;
  private BigDecimal growBounds;
}
