package com.indi.gulimall.product.mapstruct;

import com.indi.common.to.SkuReductionTO;
import com.indi.common.to.SpuBoundsTO;
import com.indi.common.to.es.SkuAttrEsModelTO;
import com.indi.common.to.es.SkuEsModelTO;
import com.indi.gulimall.product.entity.*;
import com.indi.gulimall.product.vo.*;
import com.indi.gulimall.product.vo.spusave.Attr;
import com.indi.gulimall.product.vo.spusave.Skus;
import com.indi.gulimall.product.vo.spusave.Bounds;
import com.indi.gulimall.product.vo.spusave.SpuSaveVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface ProductCovertBasic {
    ProductCovertBasic INSTANCE = Mappers.getMapper(ProductCovertBasic.class);

    AttrEntity attrVOToAttr(AttrVO attrVO);

    AttrRespVO attrToAttrRespVO(AttrEntity attr);

    AttrAttrGroupRelationEntity groupRelationVOToGroupRelation(AttrGroupRelationVO relationVO);

    AttrGroupWithAttrsVO attrGroupToAttrGroupWithAttrsVO(AttrGroupEntity attrGroup);

    SpuInfoEntity spuInfoVOToSpuInfo(SpuSaveVO spuSaveVO);

    SkuInfoEntity skuInfoVOToSkuInfo(Skus skus);

    SkuSaleAttrValueEntity skuAttrVOToSkuSaleAttrValue(Attr attr);

    SpuBoundsTO spuBoundVOToSpuBoundTO(Bounds bounds);

    SkuReductionTO skuInfoVOToSkuReductionTO(Skus skus);

    SkuAttrEsModelTO attrValueToSkuAttrEsModelTO(ProductAttrValueEntity attrValue);

    SkuEsModelTO skuInfoToSkuModelTO(SkuInfoEntity skuInfo);
}
