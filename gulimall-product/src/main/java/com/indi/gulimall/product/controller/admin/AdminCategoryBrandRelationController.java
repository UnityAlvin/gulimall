package com.indi.gulimall.product.controller.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.indi.common.utils.R;
import com.indi.gulimall.product.entity.BrandEntity;
import com.indi.gulimall.product.entity.CategoryBrandRelationEntity;
import com.indi.gulimall.product.service.CategoryBrandRelationService;
import com.indi.gulimall.product.vo.BrandVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


/**
 * 品牌分类关联
 *
 * @author UnityAlvin
 * @email UnityAlvin@qq.com
 * @date 2021-05-17 09:49:38
 */
@Api(tags = "品牌分类关联管理")
@RestController
@RequestMapping("/admin/product/categoryBrandRelation")
public class AdminCategoryBrandRelationController {
    @Autowired
    private CategoryBrandRelationService relationService;

    /**
     * 获取当前品牌关联的所有分类列表
     *
     * @param brandId
     * @return
     */
    @ApiOperation("获取当前品牌关联的所有分类列表")
    @GetMapping("/category/list/{brandId}")
    public R categoryList(@PathVariable Long brandId) {
        QueryWrapper<CategoryBrandRelationEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("brand_id", brandId);
        List<CategoryBrandRelationEntity> data = relationService.list(queryWrapper);
        return R.ok().put("data", data);
    }

    /**
     * 获取某个分类下所有品牌信息
     *
     * @param categoryId 分类id
     * @return
     */
    @ApiOperation("获取某个分类下所有品牌信息")
    @GetMapping("/brand/list/{categoryId}")
    public R getBrandList(@PathVariable Long categoryId) {
        List<BrandEntity> brandList = relationService.getBrandList(categoryId);
        List<BrandVO> brandVOS = null;
        if (brandList != null && brandList.size() > 0) {
            brandVOS = brandList.stream().map(item -> {
                BrandVO brandVO = new BrandVO();
                brandVO.setBrandId(item.getBrandId());
                brandVO.setBrandName(item.getName());
                return brandVO;
            }).collect(Collectors.toList());
        }

        return R.ok().put("data", brandVOS);
    }

    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    public R info(@PathVariable("id") Long id) {
        CategoryBrandRelationEntity categoryBrandRelation = relationService.getById(id);
        return R.ok().put("categoryBrandRelation", categoryBrandRelation);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public R save(@RequestBody CategoryBrandRelationEntity categoryBrandRelation) {
        relationService.saveDetail(categoryBrandRelation);
        return R.ok();
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public R update(@RequestBody CategoryBrandRelationEntity categoryBrandRelation) {
        relationService.updateById(categoryBrandRelation);
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids) {
        relationService.removeByIds(Arrays.asList(ids));
        return R.ok();
    }

}
