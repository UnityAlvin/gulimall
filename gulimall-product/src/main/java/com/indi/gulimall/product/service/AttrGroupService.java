package com.indi.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.indi.common.utils.PageUtils;
import com.indi.gulimall.product.entity.AttrGroupEntity;
import com.indi.gulimall.product.vo.AttrGroupWithAttrsVO;

import java.util.List;
import java.util.Map;

/**
 * 属性分组
 *
 * @author UnityAlvin
 * @email UnityAlvin@qq.com
 * @date 2021-05-17 09:49:38
 */
public interface AttrGroupService extends IService<AttrGroupEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPage(Map<String, Object> params, Long categoryId);

    void updateAttrGroup(AttrGroupEntity attrGroup);

    void removeAttrGroups(List<Long> attrGroupIds);

    List<AttrGroupWithAttrsVO> getAttrGroupWithAttrs(Long categoryId);
}

