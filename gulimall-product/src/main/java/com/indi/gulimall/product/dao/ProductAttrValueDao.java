package com.indi.gulimall.product.dao;

import com.indi.gulimall.product.entity.ProductAttrValueEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.indi.gulimall.product.vo.web.SkuItemVO.SpuAttrGroupVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * spu属性值
 * 
 * @author UnityAlvin
 * @email UnityAlvin@qq.com
 * @date 2021-05-17 09:49:38
 */
@Mapper
public interface ProductAttrValueDao extends BaseMapper<ProductAttrValueEntity> {

    List<SpuAttrGroupVO> getAttrWithGroupVO(@Param("categoryId") Long categoryId, @Param("spuId") Long spuId);
}
