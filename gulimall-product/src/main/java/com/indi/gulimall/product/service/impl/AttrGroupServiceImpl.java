package com.indi.gulimall.product.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.indi.common.utils.PageUtils;
import com.indi.common.utils.Query;
import com.indi.gulimall.product.dao.AttrAttrGroupRelationDao;
import com.indi.gulimall.product.dao.AttrGroupDao;
import com.indi.gulimall.product.entity.AttrAttrGroupRelationEntity;
import com.indi.gulimall.product.entity.AttrEntity;
import com.indi.gulimall.product.entity.AttrGroupEntity;
import com.indi.gulimall.product.mapstruct.ProductCovertBasic;
import com.indi.gulimall.product.service.AttrGroupService;
import com.indi.gulimall.product.service.AttrService;
import com.indi.gulimall.product.vo.AttrGroupWithAttrsVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service("attrGroupService")
@Slf4j
public class AttrGroupServiceImpl extends ServiceImpl<AttrGroupDao, AttrGroupEntity> implements AttrGroupService {
    @Resource
    private AttrAttrGroupRelationDao relationDao;

    @Resource
    private AttrService attrService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AttrGroupEntity> page = this.page(
                new Query<AttrGroupEntity>().getPage(params),
                new QueryWrapper<AttrGroupEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPage(Map<String, Object> params, Long categoryId) {
        QueryWrapper<AttrGroupEntity> queryWrapper = new QueryWrapper<>();

        // 如果分类id不为0
        if (categoryId != 0) {
            queryWrapper.eq("category_id", categoryId);
        }

        String key = (String) params.get("key");
        log.info("key的值{}", params.get("key"));

        // 如果查询条件不为空
        if (StringUtils.isNotEmpty(key)) {
            queryWrapper.and(wrapper -> {
                wrapper.eq("attr_group_id", key).or().like("attr_group_name", key);
            });
        }
        IPage<AttrGroupEntity> page = this.page(
                new Query<AttrGroupEntity>().getPage(params),
                queryWrapper
        );
        return new PageUtils(page);
    }

    @Override
    public void updateAttrGroup(AttrGroupEntity attrGroup) {
        AttrGroupEntity oldAttrGroup = this.getById(attrGroup.getAttrGroupId());
        // 如果修改了分组的分类，需要清空以前的关联关系
        if (!attrGroup.getCategoryId().equals(oldAttrGroup.getCategoryId())) {
            List<AttrAttrGroupRelationEntity> relations = relationDao.selectList(new QueryWrapper<AttrAttrGroupRelationEntity>()
                    .eq("attr_group_id", attrGroup.getAttrGroupId()));
            if (relations != null && relations.size() > 0) {
                relationDao.updateBatchNullRelation(relations);
            }
        }
        this.updateById(attrGroup);
    }

    @Override
    public void removeAttrGroups(List<Long> attrGroupIds) {
        baseMapper.deleteBatchIds(attrGroupIds);
        AttrAttrGroupRelationEntity relation = new AttrAttrGroupRelationEntity();
        relationDao.update(relation, new UpdateWrapper<AttrAttrGroupRelationEntity>().set("attr_group_id", null)
                .in("attr_group_id", attrGroupIds));
    }

    @Override
    public List<AttrGroupWithAttrsVO> getAttrGroupWithAttrs(Long categoryId) {
        List<AttrGroupEntity> attrGroups = this.list(new QueryWrapper<AttrGroupEntity>().eq("category_id", categoryId));
        List<AttrGroupWithAttrsVO> vos = attrGroups.stream().map(item -> {
            AttrGroupWithAttrsVO attrGroupWithAttrsVO = ProductCovertBasic.INSTANCE.attrGroupToAttrGroupWithAttrsVO(item);
            List<AttrEntity> attrs = attrService.getRelationAttr(item.getAttrGroupId());
            attrGroupWithAttrsVO.setAttrs(attrs);
            return attrGroupWithAttrsVO;
        }).collect(Collectors.toList());
        return vos;
    }

}