package com.indi.gulimall.product.vo.spusave;

import lombok.Data;

@Data
public class Attr {
  private Long attrId;
  private String attrName;
  private String attrValue;
}
