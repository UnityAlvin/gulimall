package com.indi.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.indi.common.utils.PageUtils;
import com.indi.gulimall.product.entity.AttrAttrGroupRelationEntity;
import com.indi.gulimall.product.vo.AttrGroupRelationVO;

import java.util.List;
import java.util.Map;

/**
 * 属性&属性分组关联
 *
 * @author UnityAlvin
 * @email UnityAlvin@qq.com
 * @date 2021-05-17 09:49:38
 */
public interface AttrAttrGroupRelationService extends IService<AttrAttrGroupRelationEntity> {
    PageUtils queryPage(Map<String, Object> params);

    void saveBatch(List<AttrGroupRelationVO> relationVOS);

    void deleteRelation(List<AttrGroupRelationVO> relationVOList);
}

