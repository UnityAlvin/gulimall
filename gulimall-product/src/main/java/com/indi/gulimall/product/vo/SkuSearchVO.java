package com.indi.gulimall.product.vo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class SkuSearchVO {
    private String key;
    private Long categoryId;
    private Long brandId;
    private Integer minPrice;
    private BigDecimal maxPrice;
}
