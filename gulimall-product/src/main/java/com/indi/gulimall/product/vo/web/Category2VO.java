package com.indi.gulimall.product.vo.web;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Category2VO {
    private Long category1Id;
    private List<Category3VO> category3List;
    private Long id;
    private String name;
}
