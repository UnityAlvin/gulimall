package com.indi.gulimall.product.controller.admin;

import com.indi.common.utils.PageUtils;
import com.indi.common.utils.R;
import com.indi.gulimall.product.entity.ProductAttrValueEntity;
import com.indi.gulimall.product.service.ProductAttrValueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;


/**
 * spu属性值
 *
 * @author UnityAlvin
 * @email UnityAlvin@qq.com
 * @date 2021-05-17 09:49:38
 */
@RestController
@RequestMapping("/admin/product/product-attr-value")
public class AdminProductAttrValueController {
    @Autowired
    private ProductAttrValueService productAttrValueService;

    /**
     * 根据spuId修改商品规格
     *
     * @param spuId
     * @param attrValues
     * @return
     */
    @PostMapping("/update/{spuId}")
    public R updateBySpuId(@PathVariable Long spuId, @RequestBody List<ProductAttrValueEntity> attrValues) {
        productAttrValueService.updateBySpuId(spuId, attrValues);
        return R.ok();
    }

    /**
     * 根据spuId获取spu规格
     *
     * @param spuId
     * @return
     */
    @GetMapping("/base/list-for-spu/{spuId}")
    public R listForSpu(@PathVariable Long spuId) {
        List<ProductAttrValueEntity> attrs = productAttrValueService.listBySpuId(spuId);
        return R.ok().put("data", attrs);
    }

    /**
     * 列表
     */
    @GetMapping("/list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = productAttrValueService.queryPage(params);
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    public R info(@PathVariable("id") Long id) {
        ProductAttrValueEntity productAttrValue = productAttrValueService.getById(id);
        return R.ok().put("productAttrValue", productAttrValue);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public R save(@RequestBody ProductAttrValueEntity productAttrValue) {
        productAttrValueService.save(productAttrValue);
        return R.ok();
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public R update(@RequestBody ProductAttrValueEntity productAttrValue) {
        productAttrValueService.updateById(productAttrValue);
        return R.ok();
    }

    /**
     * 删除
     */
    @DeleteMapping("/delete")
    public R delete(@RequestBody Long[] ids) {
        productAttrValueService.removeByIds(Arrays.asList(ids));
        return R.ok();
    }

}
