package com.indi.gulimall.product.feign;

import com.indi.common.utils.R;
import com.indi.gulimall.product.feign.fallback.SeckillFeignServiceFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author UnityAlvin
 * @date 2021/7/28 14:44
 */
@FeignClient(value = "gulimall-seckill",fallback = SeckillFeignServiceFallback.class)
public interface SeckillFeignService {
    /**
     * 根据skuId获取对应的秒杀信息
     * @return
     */
    @GetMapping("/web/seckill/seckill-sku-relation/{skuId}")
    R getSeckillSkuRelation(@PathVariable("skuId") Long skuId);
}
