package com.indi.gulimall.product.controller.web;

import com.indi.common.utils.R;
import com.indi.gulimall.product.entity.SpuInfoEntity;
import com.indi.gulimall.product.service.SpuInfoService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * spu信息
 *
 * @author UnityAlvin
 * @email UnityAlvin@qq.com
 * @date 2021-05-17 09:49:38
 */
@Api(tags = "spu管理")
@RestController
@RequestMapping("/web/product/spu-info")
public class SpuInfoController {
    @Autowired
    private SpuInfoService spuInfoService;

    /**
     * 通过skuId获取spu信息
     * @param skuId
     * @return
     */
    @GetMapping("/info/{skuId}")
    public R info(@PathVariable("skuId") Long skuId){
        SpuInfoEntity spuInfo = spuInfoService.getInfoBySkuId(skuId);
        return R.ok().setData(spuInfo);
    }
}
