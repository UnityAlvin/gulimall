package com.indi.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.indi.common.utils.PageUtils;
import com.indi.gulimall.product.entity.SkuSaleAttrValueEntity;
import com.indi.gulimall.product.vo.web.SkuItemVO;

import java.util.List;
import java.util.Map;

/**
 * sku销售属性&值
 *
 * @author UnityAlvin
 * @email UnityAlvin@qq.com
 * @date 2021-05-17 09:49:38
 */
public interface SkuSaleAttrValueService extends IService<SkuSaleAttrValueEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<SkuItemVO.SaleAttrVO> getSaleAttrValues(Long spuId);

    /**
     * 根据skuId获取拼接后的值
     * 格式 -> 销售属性：属性值
     * @param skuId
     * @return
     */
    List<String> getSkuSaleAttrValues(Long skuId);
}

