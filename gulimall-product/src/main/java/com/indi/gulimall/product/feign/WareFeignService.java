package com.indi.gulimall.product.feign;

import com.indi.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient("gulimall-ware")
public interface WareFeignService {
    @PostMapping("/admin/ware/ware-sku/stock-or-not")
     R stockOrNot(@RequestBody List<Long> skuIds);
}
