package com.indi.gulimall.product.vo.spusave;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class Skus {
    private String skuName;
    private BigDecimal price;
    private String skuTitle;
    private String skuSubtitle;
    private Integer fullCount;
    private BigDecimal discount;
    private Integer countStatus;
    private BigDecimal fullPrice;
    private BigDecimal reducePrice;
    private Integer priceStatus;
    private List<Attr> attr;
    private List<Images> images;
    private List<String> descar;
    private List<MemberPrice> memberPrice;
}
