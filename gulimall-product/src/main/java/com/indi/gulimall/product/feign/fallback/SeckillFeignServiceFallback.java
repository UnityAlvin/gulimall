package com.indi.gulimall.product.feign.fallback;

import com.indi.common.enums.BizCodeEnum;
import com.indi.common.utils.R;
import com.indi.gulimall.product.feign.SeckillFeignService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author UnityAlvin
 * @date 2021/8/1 12:22
 */
@Slf4j
@Component
public class SeckillFeignServiceFallback implements SeckillFeignService {
    @Override
    public R getSeckillSkuRelation(Long skuId) {
        log.info("熔断方法调用：getSeckillSkuRelation");
        return R.error(BizCodeEnum.TOO_MANY_REQUEST_EXCEPTION);
    }
}
