package com.indi.gulimall.product.dao;

import com.indi.gulimall.product.entity.AttrAttrGroupRelationEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 属性&属性分组关联
 * 
 * @author UnityAlvin
 * @email UnityAlvin@qq.com
 * @date 2021-05-17 09:49:38
 */
@Mapper
public interface AttrAttrGroupRelationDao extends BaseMapper<AttrAttrGroupRelationEntity> {
    void updateBatchNullRelation(@Param("relationList") List<AttrAttrGroupRelationEntity> relationList);
}
