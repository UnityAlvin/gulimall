package com.indi.gulimall.product.controller.web;

import com.indi.gulimall.product.service.SkuInfoService;
import com.indi.gulimall.product.vo.web.SkuItemVO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.annotation.Resource;
import java.util.concurrent.ExecutionException;

@Controller
public class ItemController {
    @Resource
    private SkuInfoService skuInfoService;

    @GetMapping("/{skuId}.html")
    public String itemPage(@PathVariable Long skuId, Model model) throws ExecutionException, InterruptedException {
        SkuItemVO skuItemVO = skuInfoService.getSkuItemVO(skuId);
        model.addAttribute("item", skuItemVO);
        return "item";
    }
}
