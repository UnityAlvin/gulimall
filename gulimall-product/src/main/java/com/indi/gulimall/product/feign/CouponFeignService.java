package com.indi.gulimall.product.feign;

import com.indi.common.to.SkuReductionTO;
import com.indi.common.to.SpuBoundsTO;
import com.indi.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("gulimall-coupon")
public interface CouponFeignService {
    @PostMapping("/admin/coupon/spu-bounds/save")
    R saveSpuBound(@RequestBody SpuBoundsTO spuBoundsTO);

    @PostMapping("/admin/coupon/sku-full-reduction/save-info")
    R saveSkuReduction(@RequestBody SkuReductionTO skuReductionTO);
}
