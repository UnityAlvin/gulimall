package com.indi.gulimall.product.vo;

import lombok.Data;

@Data
public class AttrRespVO extends AttrVO{
    private String categoryName;
    private String groupName;
    private Long[] categoryPath;
}
