package com.indi.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.indi.common.utils.PageUtils;
import com.indi.gulimall.product.entity.CategoryEntity;
import com.indi.gulimall.product.vo.web.Category2VO;

import java.util.List;
import java.util.Map;

/**
 * 商品三级分类
 *
 * @author UnityAlvin
 * @email UnityAlvin@qq.com
 * @date 2021-05-17 09:49:38
 */
public interface CategoryService extends IService<CategoryEntity> {
    PageUtils queryPage(Map<String, Object> params);

    List<CategoryEntity> listWithTree();

    void removeCategoryByIds(List<Long> catIds);

    Long[] findCategoryPath(Long categoryId);

    void updateDetail(CategoryEntity category);

    List<CategoryEntity> findLevel1Categories();

    Map<Long, List<Category2VO>> getCategoryJson();
}

