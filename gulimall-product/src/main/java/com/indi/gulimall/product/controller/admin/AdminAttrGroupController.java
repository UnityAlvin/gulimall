package com.indi.gulimall.product.controller.admin;

import com.indi.common.utils.PageUtils;
import com.indi.common.utils.R;
import com.indi.gulimall.product.entity.AttrEntity;
import com.indi.gulimall.product.entity.AttrGroupEntity;
import com.indi.gulimall.product.service.AttrAttrGroupRelationService;
import com.indi.gulimall.product.service.AttrGroupService;
import com.indi.gulimall.product.service.AttrService;
import com.indi.gulimall.product.service.CategoryService;
import com.indi.gulimall.product.vo.AttrGroupRelationVO;
import com.indi.gulimall.product.vo.AttrGroupWithAttrsVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


/**
 * 属性分组
 *
 * @author UnityAlvin
 * @email UnityAlvin@qq.com
 * @date 2021-05-17 09:49:38
 */
@Api(tags = "属性分组管理")
@RestController
@RequestMapping("/admin/product/attrGroup")
public class AdminAttrGroupController {
    @Resource
    private AttrGroupService attrGroupService;

    @Resource
    private CategoryService categoryService;

    @Resource
    private AttrService attrService;

    @Resource
    private AttrAttrGroupRelationService relationService;

    /**
     * 获取某个分类下的分组以及其关联的属性
     * @param categoryId 分类id
     * @return
     */
    @ApiOperation("获取某个分类下的分组以及其关联的属性")
    @GetMapping("/{categoryId}/withAttr")
    public R getAttrGroupWithAttrs(@PathVariable Long categoryId){
        List<AttrGroupWithAttrsVO> data = attrGroupService.getAttrGroupWithAttrs(categoryId);
        return R.ok().put("data",data);
    }

    /**
     * 单个/批量为属性分组新增关联
     * @param relationVOS   关联信息
     * @return
     */
    @PostMapping("/attr/relation")
    public R addRelation(@RequestBody List<AttrGroupRelationVO> relationVOS){
        relationService.saveBatch(relationVOS);
        return R.ok();
    }

    /**
     * 删除单个/多个分组与规格参数的关联
     *
     * @param relationVOList 规格参数列表
     * @return
     */
    @DeleteMapping("/attr/relation/delete")
    public R deleteRelation(@RequestBody List<AttrGroupRelationVO> relationVOList) {
        relationService.deleteRelation(relationVOList);
        return R.ok();
    }

    /**
     * 查出当前分组关联的所有规格参数
     *
     * @param attrGroupId 分组id
     * @return
     */
    @GetMapping("/{attrGroupId}/attr/relation")
    public R attrRelation(@PathVariable Long attrGroupId) {
        List<AttrEntity> attrList = attrService.getRelationAttr(attrGroupId);
        return R.ok().put("data", attrList);
    }

    /**
     * 查出当前分组未关联的规格参数
     * @param attrGroupId 分组id
     * @param params 查询条件
     * @return
     */
    @GetMapping("/{attrGroupId}/attr/noRelation")
    public R attrNoRelation(@PathVariable Long attrGroupId,
                            @RequestParam Map<String,Object> params){
        PageUtils page = attrService.getNoRelationAttr(attrGroupId,params);
        return R.ok().put("page",page);
    }

    /**
     * 根据分类id查询数据
     * @param params
     * @param categoryId
     * @return
     */
    @ApiOperation("根据分类id查询数据")
    @GetMapping("/list/{categoryId}")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "page", value = "当前页码", dataType = "Integer"),
            @ApiImplicitParam(paramType = "query", name = "limit", value = "每页记录数", dataType = "Integer"),
            @ApiImplicitParam(paramType = "query", name = "sidx", value = "排序字段", dataType = "String"),
            @ApiImplicitParam(paramType = "query", name = "order", value = "排序方式", dataType = "String"),
            @ApiImplicitParam(paramType = "query", name = "key", value = "条件", dataType = "String")
    })
    public R list(@ApiIgnore @RequestParam Map<String, Object> params, @PathVariable Long categoryId) {
        PageUtils page = attrGroupService.queryPage(params, categoryId);
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @GetMapping("/info/{attrGroupId}")
    public R info(@PathVariable("attrGroupId") Long attrGroupId) {
        AttrGroupEntity attrGroup = attrGroupService.getById(attrGroupId);
        Long categoryId = attrGroup.getCategoryId();
        Long[] categoryPath = categoryService.findCategoryPath(categoryId);
        attrGroup.setCategoryPath(categoryPath);
        return R.ok().put("attrGroup", attrGroup);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public R save(@RequestBody AttrGroupEntity attrGroup) {
        attrGroupService.save(attrGroup);
        return R.ok();
    }

    /**
     * 修改分组数据
     * @param attrGroup 分组信息
     * @return
     */
    @PostMapping("/update")
    public R update(@RequestBody AttrGroupEntity attrGroup) {
        attrGroupService.updateAttrGroup(attrGroup);
        return R.ok();
    }

    /**
     * 删除
     */
    @DeleteMapping("/delete")
    public R delete(@RequestBody Long[] attrGroupIds) {
        attrGroupService.removeAttrGroups(Arrays.asList(attrGroupIds));
        return R.ok();
    }

}
