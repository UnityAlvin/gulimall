package com.indi.gulimall.product.vo;

import com.indi.gulimall.product.dto.SeckillSkuRelationDTO;
import lombok.Data;

/**
 * 需要存到redis的秒杀商品相关数据
 *
 * @author UnityAlvin
 * @date 2021/7/27 11:02
 */
@Data
public class SeckillSkuRedisVO {
    private SeckillSkuRelationDTO seckillSkuRelationDTO;    // 秒杀sku信息
    private Long startTime; // 当前商品的秒杀开始时间
    private Long endTime;   // 当前商品的秒杀结束时间
    private String randomCode;  // 秒杀商品的随机码
}
