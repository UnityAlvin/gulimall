package com.indi.gulimall.product.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.indi.common.utils.PageUtils;
import com.indi.common.utils.Query;
import com.indi.gulimall.product.dao.CategoryDao;
import com.indi.gulimall.product.entity.CategoryEntity;
import com.indi.gulimall.product.service.CategoryBrandRelationService;
import com.indi.gulimall.product.service.CategoryService;
import com.indi.gulimall.product.vo.web.Category2VO;
import com.indi.gulimall.product.vo.web.Category3VO;
import org.apache.commons.lang.StringUtils;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;


@Service("categoryService")
public class CategoryServiceImpl extends ServiceImpl<CategoryDao, CategoryEntity> implements CategoryService {
    @Autowired
    private CategoryBrandRelationService relationService;

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Resource
    private RedissonClient redissonClient;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryEntity> page = this.page(
                new Query<CategoryEntity>().getPage(params),
                new QueryWrapper<CategoryEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<CategoryEntity> listWithTree() {
        List<CategoryEntity> entities = baseMapper.selectList(null);

        // 查询1级分类以及其下的所有子分类
        List<CategoryEntity> categoryEntityList = entities.stream().filter(categoryEntity ->
                categoryEntity.getParentCid() == 0
        ).map(categoryEntity -> {
            categoryEntity.setChildrenList(getChildrenList(categoryEntity, entities));
            return categoryEntity;
        }).sorted((o1, o2) -> (o1.getSort() == null ? 0 : o1.getSort()) - (o2.getSort() == null ? 0 : o2.getSort())
        ).collect(Collectors.toList());

        return categoryEntityList;
    }

    @Override
    public void removeCategoryByIds(List<Long> catIds) {
        // TODO:1、检查当前要删除的分类，是否被别的地方引用

        baseMapper.deleteBatchIds(catIds);
    }

    @Override
    public Long[] findCategoryPath(Long categoryId) {
        List<Long> categoryList = new ArrayList<>();
        List<Long> categoryPath = findParentCategory(categoryId, categoryList);
        Collections.reverse(categoryPath);
        return categoryPath.toArray(new Long[categoryPath.size()]);
    }

    //    @Caching(evict = {
//            @CacheEvict(value = "category", key = "'level1Categories'"),
//            @CacheEvict(value = "category", key = "'categoryJson'"),
//    })
    @CacheEvict(value = "category", allEntries = true)
    @Transactional
    @Override
    public void updateDetail(CategoryEntity category) {
        this.updateById(category);
        if (StringUtils.isNotEmpty(category.getName())) {
            relationService.updateCategory(category.getCatId(), category.getName());

            // TODO 更新其它关联
        }
    }

    /**
     * 查询一级分类
     * @return
     */
    @Override
    @Cacheable(value = {"category"}, key = "'level1Categories'")
    public List<CategoryEntity> findLevel1Categories() {
        long l = System.currentTimeMillis();
        List<CategoryEntity> categories = this.list(new QueryWrapper<CategoryEntity>().eq("parent_cid", 0L));
        System.out.println("花费时间：" + (System.currentTimeMillis() - l));
        return categories;
    }

    private List<CategoryEntity> listByPrentCid(List<CategoryEntity> categories, Long parentCid) {
        List<CategoryEntity> finalCategories = categories.stream().filter(category ->
                category.getParentCid().equals(parentCid)).collect(Collectors.toList());
        return finalCategories;
    }

    /**
     * 正常查询redis缓存版本
     * @return
     */
//    @Override
//    public Map<Long, List<Category2VO>> getCategoryJson() {
//        String categoryJson = stringRedisTemplate.opsForValue().get("category.json");
//        if (StringUtils.isEmpty(categoryJson)) {
//            // 第一遍先从数据库中获取到数据
//            System.out.println("缓存不命中，将要查询数据库");
//            Map<Long, List<Category2VO>> categoryJsonMap = getCategoryJsonWithRedisLock();
//
//            // 返回数据
//            return categoryJsonMap;
//        }
//
//        // 不为空，则将JSON数据反序列化成页面需要的数据
//        System.out.println("缓存命中，直接返回");
//        return JSON.parseObject(categoryJson, new TypeReference<Map<Long, List<Category2VO>>>() {
//        });
//    }

    /**
     * 查询全部分类
     * 使用SpringCache缓存版本
     * 只需要操作数据库，不需要关心缓存，一个注解就够了
     * @return
     */
    @Cacheable(value = {"category"}, key = "'categoryJson'")
    @Override
    public Map<Long, List<Category2VO>> getCategoryJson() {
        // 查出所有分类
        List<CategoryEntity> allCategories = this.list();
        List<CategoryEntity> l1Categories = listByPrentCid(allCategories, 0L);

        Map<Long, List<Category2VO>> categoryMap = l1Categories.stream().collect(Collectors.toMap(k1 -> k1.getCatId(), v1 -> {
            List<CategoryEntity> l2Categories = listByPrentCid(allCategories, v1.getCatId());
            List<Category2VO> category2VOs = null;

            if (l2Categories != null && l2Categories.size() > 0) {
                category2VOs = l2Categories.stream().map(l2 -> {
                    // 根据当前2级分类查出所有3级分类
                    List<CategoryEntity> l3Categories = listByPrentCid(allCategories, l2.getCatId());
                    List<Category3VO> category3VOs = null;
                    if (l3Categories != null && l3Categories.size() > 0) {
                        category3VOs = l3Categories.stream().map(l3 -> new Category3VO(l2.getCatId(),
                                l3.getCatId(), l3.getName())).collect(Collectors.toList());
                    }

                    return new Category2VO(v1.getCatId(), category3VOs, l2.getCatId(), l2.getName());
                }).collect(Collectors.toList());
            }
            return category2VOs;

        }));

        return categoryMap;
    }

    /**
     * 使用本地锁解决缓存击穿
     *
     * @return
     */
    private Map<Long, List<Category2VO>> getCategoryJsonWithLocalLock() {
        synchronized (this) {
            return getCategoryJsonFromDB();
        }
    }

    /**
     * 使用分布式锁解决缓存击穿
     *
     * @return
     */
    private Map<Long, List<Category2VO>> getCategoryJsonWithRedisLock() {
        String token = UUID.randomUUID().toString();
        // 去redis占锁，原子加锁
        Boolean lock = stringRedisTemplate.opsForValue().setIfAbsent("lock", token, 300, TimeUnit.SECONDS);
        if (lock) {
            Map<Long, List<Category2VO>> categoryMap;
            System.out.println("获取分布式锁成功");
            try {
                // 加锁成功，执行业务
                categoryMap = getCategoryJsonFromDB();
            } finally {
                // 这段脚本的意思是，如果获取key对应的value是传过来的值，那就调用删除方法返回1，否则返回0
                String script = "if redis.call('get', KEYS[1]) == ARGV[1] then return redis.call('del', KEYS[1]) else return 0 end";
                // 原子删锁
                Long result = stringRedisTemplate.execute(new DefaultRedisScript<>(script, Long.class),
                        Arrays.asList("lock"), token);
            }

            return categoryMap;
        } else {
            // 加锁失败，重试，使用自旋的方式，模仿本地sync监听锁
            System.out.println("获取分布式锁失败...等待重试");
            try {
                Thread.sleep(200);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return getCategoryJsonWithRedisLock();
        }
    }

    /**
     * 使用RedissonLock解决缓存击穿
     *
     * @return
     */
    private Map<Long, List<Category2VO>> getCategoryJsonWithRedissonLock() {
        RLock rLock = redissonClient.getReadWriteLock("categoryJsonLock").readLock();
        Map<Long, List<Category2VO>> categoryMap;
        try {
            rLock.lock();
            categoryMap = getCategoryJsonFromDB();
        } finally {
            rLock.unlock();
        }
        return categoryMap;
    }


    private Map<Long, List<Category2VO>> getCategoryJsonFromDB() {
        // 得到锁之后，应该再去缓存中确定一次，如果缓存有，就直接返回，没有再去查询
        String categoryJson = stringRedisTemplate.opsForValue().get("category.json");
        if (StringUtils.isNotEmpty(categoryJson)) {
            System.out.println("从缓存中获取数据");
            return JSON.parseObject(categoryJson, new TypeReference<Map<Long, List<Category2VO>>>() {
            });
        }
        System.out.println("查询了数据库");
        // 查出所有分类
        List<CategoryEntity> allCategories = this.list();
        List<CategoryEntity> l1Categories = listByPrentCid(allCategories, 0L);

        Map<Long, List<Category2VO>> categoryMap = l1Categories.stream().collect(Collectors.toMap(k1 -> k1.getCatId(), v1 -> {
            List<CategoryEntity> l2Categories = listByPrentCid(allCategories, v1.getCatId());
            List<Category2VO> category2VOs = null;

            if (l2Categories != null && l2Categories.size() > 0) {
                category2VOs = l2Categories.stream().map(l2 -> {
                    // 根据当前2级分类查出所有3级分类
                    List<CategoryEntity> l3Categories = listByPrentCid(allCategories, l2.getCatId());
                    List<Category3VO> category3VOs = null;
                    if (l3Categories != null && l3Categories.size() > 0) {
                        category3VOs = l3Categories.stream().map(l3 -> new Category3VO(l2.getCatId(),
                                l3.getCatId(), l3.getName())).collect(Collectors.toList());
                    }

                    return new Category2VO(v1.getCatId(), category3VOs, l2.getCatId(), l2.getName());
                }).collect(Collectors.toList());
            }
            return category2VOs;

        }));

        // 然后序列化成JSON字符串
        String newCategoryJson = JSON.toJSONString(categoryMap);

        // 放入缓存中
        stringRedisTemplate.opsForValue().set("category.json", newCategoryJson, 1, TimeUnit.DAYS);

        return categoryMap;
    }

    private List<Long> findParentCategory(Long categoryId, List<Long> categoryList) {
        categoryList.add(categoryId);
        CategoryEntity category = this.getById(categoryId);
        if (category.getParentCid() != 0) {
            findParentCategory(category.getParentCid(), categoryList);
        }
        return categoryList;
    }


    private List<CategoryEntity> getChildrenList(CategoryEntity root, List<CategoryEntity> all) {
        List<CategoryEntity> childrenList = all.stream().filter(categoryEntity -> {
            // 当前分类的父id等于要查找分类的id，就说明当前分类是要查找分类的子分类
            return categoryEntity.getParentCid() == root.getCatId();
        }).map(categoryEntity -> {
            // 开始递归查找匹配到的分类
            categoryEntity.setChildrenList(getChildrenList(categoryEntity, all));
            return categoryEntity;
        }).sorted((o1, o2) -> (o1.getSort() == null ? 0 : o1.getSort()) - (o2.getSort() == null ? 0 : o2.getSort())
        ).collect(Collectors.toList());

        return childrenList;
    }
}