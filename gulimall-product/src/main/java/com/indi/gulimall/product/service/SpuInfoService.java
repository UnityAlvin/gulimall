package com.indi.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.indi.common.utils.PageUtils;
import com.indi.gulimall.product.entity.SpuInfoEntity;
import com.indi.gulimall.product.vo.SpuSearchVO;
import com.indi.gulimall.product.vo.spusave.SpuSaveVO;

import java.util.Map;

/**
 * spu信息
 *
 * @author UnityAlvin
 * @email UnityAlvin@qq.com
 * @date 2021-05-17 09:49:38
 */
public interface SpuInfoService extends IService<SpuInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveSpuInfo(SpuSaveVO spuSaveVO);

    PageUtils queryPageByCondition(Map<String, Object> params, SpuSearchVO spuSearchVO);

    void spuUp(Long spuId);

    void updateSpuStatus(Long spuId, int code);

    /**
     * 通过skuId获取spu信息
     * @param skuId
     * @return
     */
    SpuInfoEntity getInfoBySkuId(Long skuId);
}

