package com.indi.gulimall.product.vo;

import lombok.Data;

@Data
public class SpuSearchVO {
    private Long brandId;
    private Long categoryId;
    private String key;
    private Integer status;
}
