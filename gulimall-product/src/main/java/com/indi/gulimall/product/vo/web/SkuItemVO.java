package com.indi.gulimall.product.vo.web;

import com.indi.gulimall.product.entity.SkuImagesEntity;
import com.indi.gulimall.product.entity.SkuInfoEntity;
import com.indi.gulimall.product.entity.SpuInfoDescEntity;
import com.indi.gulimall.product.vo.SeckillSkuRedisVO;
import lombok.Data;

import java.util.List;

@Data
public class SkuItemVO {
    private Boolean stockOrNot;  // 有没有货
    private SkuInfoEntity skuInfo;  // sku基本信息获取 skuInfo
    private SpuInfoDescEntity spuInfoDesc;  // spu的介绍
    private List<SkuImagesEntity> skuImages;    // sku图片信息	skuImages
    private List<SaleAttrVO> saleAttrVOs;   // spu销售属性组合
    private List<SpuAttrGroupVO> attrGroupVOs;  // 获取spu的规格参数信息
    private SeckillSkuRedisVO seckillInfo;   // 当前商品的秒杀信息

    /**
     * 销售属性
     */
    @Data
    public static class SaleAttrVO{
        private Long attrId;
        private String attrName;
        private List<SaleAttrWithSkuVO> saleAttrWithSkuVOs;
    }

    @Data
    public static class SaleAttrWithSkuVO{
        private String attrValue;
        private String skuIds;
    }
    
    /**
     * 规格参数分组
     */
    @Data
    public static class SpuAttrGroupVO{
        private String attrGroupName;
        private List<BaseAttrVO> baseAttrVOs;
    }

    /**
     * 规格参数
     */
    @Data
    public static class BaseAttrVO{
        private String attrName;
        private String attrValue;
    }

}
