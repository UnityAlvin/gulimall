package com.indi.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.indi.common.utils.PageUtils;
import com.indi.gulimall.product.entity.SkuInfoEntity;
import com.indi.gulimall.product.vo.SkuSearchVO;
import com.indi.gulimall.product.vo.web.SkuItemVO;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * sku信息
 *
 * @author UnityAlvin
 * @email UnityAlvin@qq.com
 * @date 2021-05-17 09:49:38
 */
public interface SkuInfoService extends IService<SkuInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPageByCondition(Map<String, Object> params, SkuSearchVO skuSearchVO);

    List<SkuInfoEntity> listBySpuId(Long spuId);

    /**
     * 获取商品详情所需数据
     * @param skuId
     * @return
     * @throws ExecutionException
     * @throws InterruptedException
     */
    SkuItemVO getSkuItemVO(Long skuId) throws ExecutionException, InterruptedException;
}

