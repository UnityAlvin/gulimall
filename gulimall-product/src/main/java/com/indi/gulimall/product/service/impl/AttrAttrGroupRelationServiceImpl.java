package com.indi.gulimall.product.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.indi.common.utils.PageUtils;
import com.indi.common.utils.Query;
import com.indi.gulimall.product.dao.AttrAttrGroupRelationDao;
import com.indi.gulimall.product.entity.AttrAttrGroupRelationEntity;
import com.indi.gulimall.product.mapstruct.ProductCovertBasic;
import com.indi.gulimall.product.service.AttrAttrGroupRelationService;
import com.indi.gulimall.product.vo.AttrGroupRelationVO;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service("attrAttrGroupRelationService")
public class AttrAttrGroupRelationServiceImpl extends ServiceImpl<AttrAttrGroupRelationDao, AttrAttrGroupRelationEntity> implements AttrAttrGroupRelationService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AttrAttrGroupRelationEntity> page = this.page(
                new Query<AttrAttrGroupRelationEntity>().getPage(params),
                new QueryWrapper<AttrAttrGroupRelationEntity>()
        );

        return new PageUtils(page);
    }

    @Resource
    private AttrAttrGroupRelationDao relationDao;

    @Override
    public void saveBatch(List<AttrGroupRelationVO> relationVOS) {
        List<AttrAttrGroupRelationEntity> relations = relationVOS.stream().map(item -> {
            AttrAttrGroupRelationEntity relation = ProductCovertBasic.INSTANCE.groupRelationVOToGroupRelation(item);
            return relation;
        }).collect(Collectors.toList());

        List<Long> attrIds = relationVOS.stream().map(item -> item.getAttrId()).collect(Collectors.toList());
        List<AttrAttrGroupRelationEntity> relationList = baseMapper.selectList(new QueryWrapper<AttrAttrGroupRelationEntity>()
                .in("attr_id", attrIds));
        if (relationList != null && relationList.size() > 0) {
            for (AttrAttrGroupRelationEntity newRelation : relations) {
                for (AttrAttrGroupRelationEntity oldRelation : relationList) {
                    if (oldRelation.getAttrId() == newRelation.getAttrId()) {
                        oldRelation.setAttrGroupId(newRelation.getAttrGroupId());
                    }
                }
            }
            updateBatchById(relationList);
        } else {
            this.saveBatch(relations);
        }
    }

    @Override
    public void deleteRelation(List<AttrGroupRelationVO> relationVOList) {
        List<AttrAttrGroupRelationEntity> relationList = relationVOList.stream().map(relationVO -> {
            AttrAttrGroupRelationEntity relation = ProductCovertBasic.INSTANCE.groupRelationVOToGroupRelation(relationVO);
            return relation;
        }).collect(Collectors.toList());

        relationDao.updateBatchNullRelation(relationList);
    }
}