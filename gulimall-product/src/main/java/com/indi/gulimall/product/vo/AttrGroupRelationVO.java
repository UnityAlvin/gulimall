package com.indi.gulimall.product.vo;

import lombok.Data;

@Data
public class AttrGroupRelationVO {
    private Long attrId;
    private Long attrGroupId;
}
