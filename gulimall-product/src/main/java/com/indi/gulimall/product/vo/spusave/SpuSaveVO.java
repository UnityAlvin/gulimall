package com.indi.gulimall.product.vo.spusave;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class SpuSaveVO {
    private String spuName;
    private String spuDescription;
    private Long categoryId;
    private Long brandId;
    private BigDecimal weight;
    private Integer publishStatus;
    private Bounds bounds;
    private List<String> descriptions;
    private List<String> images;
    private List<BaseAttrs> baseAttrs;
    private List<Skus> skus;
}
