package com.indi.gulimall.product.controller.admin;

import com.indi.common.utils.PageUtils;
import com.indi.common.utils.R;
import com.indi.gulimall.product.service.AttrService;
import com.indi.gulimall.product.vo.AttrRespVO;
import com.indi.gulimall.product.vo.AttrVO;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;


/**
 * 商品属性
 *
 * @author UnityAlvin
 * @email UnityAlvin@qq.com
 * @date 2021-05-17 09:49:38
 */
@Api(tags = "商品属性管理")
@RestController
@RequestMapping("/admin/product/attr")
public class AdminAttrController {
    @Autowired
    private AttrService attrService;

    /**
     * 列表
     */
    @GetMapping("/{attrType}/list/{categoryId}")
    public R list(@RequestParam Map<String, Object> params,
                  @PathVariable Long categoryId,
                  @PathVariable String attrType) {
        PageUtils page = attrService.queryBaseAttrPage(params, categoryId, attrType);
        return R.ok().put("page", page);
    }

    /**
     * 信息
     */
    @GetMapping("/info/{attrId}")
    public R info(@PathVariable("attrId") Long attrId) {
        AttrRespVO attrRespVO = attrService.getAttrInfo(attrId);
        return R.ok().put("attr", attrRespVO);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public R save(@RequestBody AttrVO attrVO) {
        attrService.saveAttr(attrVO);
        return R.ok();
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public R update(@RequestBody AttrVO attrVO) {
        attrService.updateAttr(attrVO);
        return R.ok();
    }

    /**
     * 删除
     */
    @DeleteMapping("/delete")
    public R delete(@RequestBody Long[] attrIds) {
        attrService.removeAttrs(Arrays.asList(attrIds));
        return R.ok();
    }

}
