package com.indi.gulimall.product.controller.admin;

import com.indi.common.utils.R;
import com.indi.gulimall.product.entity.CategoryEntity;
import com.indi.gulimall.product.service.CategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;


/**
 * 商品三级分类
 *
 * @author UnityAlvin
 * @email UnityAlvin@qq.com
 * @date 2021-05-17 09:49:38
 */
@Api(tags = "分类管理")
@RestController
@RequestMapping("/admin/product/category")
public class AdminCategoryController {
    @Autowired
    private CategoryService categoryService;

    @ApiOperation("树形分类列表")
    @GetMapping("/list/tree")
    public R listWithTree() {
        List<CategoryEntity> entities = categoryService.listWithTree();
        return R.ok().put("data", entities);
    }

    @ApiOperation("删除分类")
    @DeleteMapping("/delete")
    public R delete(@RequestBody Long[] catIds) {
        categoryService.removeCategoryByIds(Arrays.asList(catIds));
        return R.ok();
    }

    @ApiOperation("添加分类")
    @PostMapping("/save")
    public R save(@RequestBody CategoryEntity category) {
        categoryService.save(category);
        return R.ok();
    }

    @ApiOperation("根据id获取分类信息")
    @GetMapping("/info/{catId}")
    public R info(@PathVariable("catId") Long catId) {
        CategoryEntity category = categoryService.getById(catId);
        return R.ok().put("data", category);
    }

    @ApiOperation("修改分类")
    @PostMapping("/update")
    public R update(@RequestBody CategoryEntity category) {
        categoryService.updateDetail(category);
        return R.ok();
    }

    @ApiOperation("更新排序")
    @PostMapping("/update/sort")
    public R updateSort(@RequestBody CategoryEntity[] category) {
        categoryService.updateBatchById(Arrays.asList(category));
        return R.ok();
    }
}
