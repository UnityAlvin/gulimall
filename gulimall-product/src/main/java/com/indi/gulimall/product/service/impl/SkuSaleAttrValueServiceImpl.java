package com.indi.gulimall.product.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.indi.common.utils.PageUtils;
import com.indi.common.utils.Query;
import com.indi.gulimall.product.dao.SkuSaleAttrValueDao;
import com.indi.gulimall.product.entity.SkuSaleAttrValueEntity;
import com.indi.gulimall.product.service.SkuSaleAttrValueService;
import com.indi.gulimall.product.vo.web.SkuItemVO;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;


@Service("skuSaleAttrValueService")
public class SkuSaleAttrValueServiceImpl extends ServiceImpl<SkuSaleAttrValueDao, SkuSaleAttrValueEntity> implements SkuSaleAttrValueService {
    @Resource
    private SkuSaleAttrValueDao skuSaleAttrValueDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SkuSaleAttrValueEntity> page = this.page(
                new Query<SkuSaleAttrValueEntity>().getPage(params),
                new QueryWrapper<SkuSaleAttrValueEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<SkuItemVO.SaleAttrVO> getSaleAttrValues(Long spuId) {
        List<SkuItemVO.SaleAttrVO> saleAttrVOs = skuSaleAttrValueDao.getSaleAttrValues(spuId);
        return saleAttrVOs;
    }

    @Override
    public List<String> getSkuSaleAttrValues(Long skuId) {
        List<String> skuSaleAttrValues =  skuSaleAttrValueDao.getSkuSaleAttrValues(skuId);
        return skuSaleAttrValues;
    }

}