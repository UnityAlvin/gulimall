package com.indi.gulimall.product.feign;

import com.indi.common.to.es.SkuEsModelTO;
import com.indi.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient("gulimall-search")
public interface EsFeignService {
    @PostMapping("/admin/search/save/product")
    R productStatusUp(@RequestBody List<SkuEsModelTO> skuEsModelTOs);
}
