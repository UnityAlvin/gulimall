package com.indi.gulimall.product.vo.spusave;

import lombok.Data;

@Data
public class Images {
  private String imgUrl;
  private Integer defaultImg;
}
