package com.indi.gulimall.product.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.indi.common.valid.AddGroup;
import com.indi.common.valid.ListValue;
import com.indi.common.valid.UpdateGroup;
import lombok.Data;

import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * 品牌
 *
 * @author UnityAlvin
 * @email UnityAlvin@qq.com
 * @date 2021-05-17 09:49:38
 */
@Data
@TableName("pms_brand")
public class BrandEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 品牌id
     */
    @NotNull(message = "修改必须指定品牌", groups = UpdateGroup.class)
    @Null(message = "新增不能指定id", groups = AddGroup.class)
    @TableId
    private Long brandId;
    /**
     * 品牌名√
     */
    @NotBlank(message = "品牌名不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String name;
    /**
     * 品牌logo地址√
     */
    @NotBlank(message = "logo地址不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String logo;
    /**
     * 介绍
     */
    @NotBlank(message = "品牌介绍不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String descript;
    /**
     * 显示状态[0-不显示；1-显示]
     * <p>
     * 需要为其单独添加一个组，只在更新显示状态用
     * 如果不单独指定，与update使用一个的话，只更新显示状态，不更新其它字段，会触发其它不允许为空的校验
     */
    @NotNull(message = "显示状态只能为0、1", groups = {AddGroup.class, UpdateGroup.class, UpdateStatusGroup.class})
    @ListValue(values = {0, 1}, groups = {AddGroup.class, UpdateGroup.class, UpdateStatusGroup.class})
    private Integer showStatus;
    /**
     * 检索首字母
     */
    @Pattern(regexp = "^[a-zA-Z]$", message = "检索首字母必须是一个字母", groups = {AddGroup.class, UpdateGroup.class})
    private String firstLetter;
    /**
     * 排序
     */
    @NotNull(message = "排序必须为正整数", groups = {AddGroup.class, UpdateGroup.class})
    @Min(value = 0, message = "排序必须为正整数", groups = {AddGroup.class, UpdateGroup.class})
    private Integer sort;

    /**
     * 逻辑删除
     */
    @TableLogic
    @TableField("is_deleted")
    private Integer deleted;

    /**
     * 更新状态的分组
     */
    public interface UpdateStatusGroup { }
}
