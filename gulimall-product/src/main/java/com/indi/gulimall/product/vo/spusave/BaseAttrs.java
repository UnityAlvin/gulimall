package com.indi.gulimall.product.vo.spusave;

import lombok.Data;

@Data
public class BaseAttrs {
  private Long attrId;
  private String attrValues;
  private Integer showDesc;
}
