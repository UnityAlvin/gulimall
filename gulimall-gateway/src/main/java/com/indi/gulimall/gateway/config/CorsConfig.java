package com.indi.gulimall.gateway.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;

@Configuration
public class CorsConfig {

    /**
     * 跨域配置
     * @return
     */
    @Bean
    public CorsWebFilter corsWebFilter(){
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration corsConfiguration = new CorsConfiguration();

        // 允许哪些头部请求跨域：全部
        corsConfiguration.addAllowedHeader("*");
        // 允许哪些请求方式跨域：所有
        corsConfiguration.addAllowedMethod("*");
        // 允许哪些请求来源跨域：任意
        corsConfiguration.addAllowedOrigin("*");
        // 是否允许携带cookie跨域：允许
        corsConfiguration.setAllowCredentials(true);

        source.registerCorsConfiguration("/**",corsConfiguration);
        return new CorsWebFilter(source);
    }
}
