import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.codec.digest.Md5Crypt;
import org.junit.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class CommonTest {
    @Test
    public void testMd5() {
        // 普通MD5：e10adc3949ba59abbe56e057f20f883e
        String s = DigestUtils.md5Hex("123456");
        System.out.println("普通MD5：" + s);

        // 默认盐值加密：$1$mfFO0ZtW$XI6iWQ1H.sg9mwq0NSBgD.
        // 加密方式：$1$+随机的8位字符
        String md5Crypt = Md5Crypt.md5Crypt("123456".getBytes());
        System.out.println("默认盐值加密：" + md5Crypt);

        // 自定义盐值加密
        String md5Crypt1 = Md5Crypt.md5Crypt("123456".getBytes(), "$1$1" + System.currentTimeMillis());
        System.out.println("自定义盐值加密：" + md5Crypt1);

        // yyds BCryptPasswordEncoder盐值加密：$2a$10$tsxedWVtV1SuaDIqUp4K2e8eK16dCq7DoNx.ovq7G3UTw44c.hyQe
        // 方法内部融合了盐值
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encode = passwordEncoder.encode("123456");
        System.out.println("BCryptPasswordEncoder盐值加密：" + encode);

        boolean matchResult = passwordEncoder.matches("123456", "$2a$10$tsxedWVtV1SuaDIqUp4K2e8eK16dCq7DoNx.ovq7G3UTw44c.hyQe");
        System.out.println("校验结果：" + matchResult);
    }
}
