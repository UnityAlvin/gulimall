package com.indi.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 错误码和错误信息定义类
 * 1. 错误码定义规则为5为数字
 * 2. 前两位表示业务场景，最后三位表示错误码。例如：100001。10:通用 001:系统未知异常
 * 3. 维护错误码后需要维护错误描述，将他们定义为枚举形式
 * 错误码列表：
 * 10: 通用
 * 001：参数格式校验
 * 11: 商品
 * 12: 订单
 * 13: 购物车
 * 14: 仓储
 * 15: 会员
 */
@Getter
@AllArgsConstructor
public enum BizCodeEnum {
    // 通用异常
    UNKNOWN_EXCEPTION(10000, "系统未知异常"),
    VALID_EXCEPTION(10001, "参数格式校验失败"),
    SMS_CODE_EXCEPTION(10002, "获取验证码过于频繁，请稍候重试"),
    OAUTH2_GITEE_EXCEPTION(10003, "Gitee授权失败"),
    FEIGN_SERVICE_EXCEPTION(10004, "远程服务调用失败"),
    TOO_MANY_REQUEST_EXCEPTION(10005, "请求次数过多，请勿重复请求"),

    // 商品异常
    PRODUCT_UP_EXCEPTION(11000, "商品上架异常"),

    // 仓储异常
    MERGE_PURCHASE_EXCEPTION(14000, "要合并的采购需求中包含已合并的采购需求，合并失败"),
    HAS_STOCK_EXCEPTION(14001, "库存不足，下单失败"),

    // 会员异常
    USERNAME_EXIST_EXCEPTION(15000, "用户名已存在"),
    PHONE_NUM_EXIST_EXCEPTION(15001, "手机号已存在"),
    LOGIN_EXCEPTION(15002, "用户名或密码错误");

    private int code;       // 响应状态码
    private String message;     // 响应信息
}
