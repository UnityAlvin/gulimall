package com.indi.common.dto;

/**
 * @author UnityAlvin
 * @date 2021/7/27 11:03
 */

import lombok.Data;

import java.math.BigDecimal;

/**
 * 秒杀场次关联的秒杀商品
 */
@Data
public class SeckillSkuRelationDTO {
    private Long id;
    private Long promotionId;
    private Long promotionSessionId;
    private Long skuId;
    private BigDecimal seckillPrice;
    private Integer seckillCount;
    private Integer seckillLimit;
    private Integer seckillSort;
}
