package com.indi.common.dto;

import lombok.Data;

@Data
public class UserInfoDTO {
    private Long userId;    // 已登录用户的id
    private String userKey; // 临时用户的cookie键
    private Boolean tempUser = false;   // 如果cookie里面有userkey则为true，没有则为false
}
