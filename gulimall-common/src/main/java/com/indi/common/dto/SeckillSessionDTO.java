package com.indi.common.dto;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 优惠服务返回的秒杀活动场次以及对应商品
 *
 * @author UnityAlvin
 * @email UnityAlvin@qq.com
 * @date 2021-05-17 09:30:16
 */
@Data
public class SeckillSessionDTO {
    private Long id;
    private String name;
    private Date startTime;
    private Date endTime;
    private Integer status;
    private Date createTime;
    private List<SeckillSkuRelationDTO> relations;
}
