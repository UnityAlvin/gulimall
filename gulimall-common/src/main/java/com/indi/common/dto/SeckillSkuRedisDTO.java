package com.indi.common.dto;

import lombok.Data;

import java.util.List;

/**
 * 需要存到redis的秒杀商品相关数据
 *
 * @author UnityAlvin
 * @date 2021/7/27 11:02
 */
@Data
public class SeckillSkuRedisDTO {
    private SkuInfoDTO skuInfoDTO;  // 基本sku信息
    private List<String> skuAttr;  // 商品的销售属性
    private SeckillSkuRelationDTO seckillSkuRelationDTO;    // 秒杀sku信息
    private Long startTime; // 当前商品的秒杀开始时间
    private Long endTime;   // 当前商品的秒杀结束时间
    private String randomCode;  // 秒杀商品的随机码
}
