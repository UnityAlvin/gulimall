package com.indi.common.exception;

import com.indi.common.enums.BizCodeEnum;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BusinessException extends RuntimeException {
    private Integer code;
    private String message;

    /**
     * @param message 错误消息
     */
    public BusinessException(String message) {
        this.message = message;
    }

    /**
     * @param message 错误消息
     * @param code    错误码
     */
    public BusinessException(String message, Integer code) {
        this.message = message;
        this.code = code;
    }

    /**
     * @param message 错误消息
     * @param code    错误码
     * @param cause   原始异常对象
     */
    public BusinessException(String message, Integer code, Throwable cause) {
        super(cause);
        this.message = message;
        this.code = code;
    }

    /**
     * @param bizCodeEnum 接收枚举类型
     */
    public BusinessException(BizCodeEnum bizCodeEnum) {
        this.message = bizCodeEnum.getMessage();
        this.code = bizCodeEnum.getCode();
    }

    /**
     * @param bizCodeEnum 接收枚举类型
     * @param cause          原始异常对象
     */
    public BusinessException(BizCodeEnum bizCodeEnum, Throwable cause) {
        super(cause);
        this.message = bizCodeEnum.getMessage();
        this.code = bizCodeEnum.getCode();
    }
}
