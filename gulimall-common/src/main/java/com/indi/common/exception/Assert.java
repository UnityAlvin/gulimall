package com.indi.common.exception;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.indi.common.enums.BizCodeEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import java.util.List;

@Slf4j
public class Assert {
    /**
     * 断言对象不为空，为空则抛异常
     *
     * @param obj
     * @param bizCodeEnum
     */
    public static void notNull(Object obj, BizCodeEnum bizCodeEnum) {
        if (obj == null) {
            log.info("obj is null...............................");
            throw new BusinessException(bizCodeEnum);
        }
    }

    /**
     * 断言对象为空
     * 如果对象obj不为空，则抛出异常
     *
     * @param object
     * @param bizCodeEnum
     */
    public static void isNull(Object object, BizCodeEnum bizCodeEnum) {
        if (object != null) {
            log.info("obj is not null......");
            throw new BusinessException(bizCodeEnum);
        }
    }

    /**
     * 断言集合为空
     * 如果集合不为空，则抛出异常
     *
     * @param list
     * @param bizCodeEnum
     */
    public static void isNullList(List list, BizCodeEnum bizCodeEnum) {
        if (CollectionUtils.isNotEmpty(list)) {
            log.info("list is not null......");
            throw new BusinessException(bizCodeEnum);
        }
    }

    /**
     * 断言集合不为空
     * 如果集合为空，则抛出异常
     *
     * @param list
     * @param bizCodeEnum
     */
    public static void notEmptyList(List list, BizCodeEnum bizCodeEnum) {
        if (CollectionUtils.isEmpty(list)) {
            log.info("list is null......");
            throw new BusinessException(bizCodeEnum);
        }
    }

    /**
     * 断言表达式为真
     * 如果不为真，则抛出异常
     *
     * @param expression 是否成功
     */
    public static void isTrue(boolean expression, BizCodeEnum bizCodeEnum) {
        if (!expression) {
            log.info("fail...............");
            throw new BusinessException(bizCodeEnum);
        }
    }

    /**
     * 断言两个对象不相等
     * 如果相等，则抛出异常
     *
     * @param m1
     * @param m2
     * @param bizCodeEnum
     */
    public static void notEquals(Object m1, Object m2, BizCodeEnum bizCodeEnum) {
        if (m1.equals(m2)) {
            log.info("equals...............");
            throw new BusinessException(bizCodeEnum);
        }
    }

    /**
     * 断言两个对象相等
     * 如果不相等，则抛出异常
     *
     * @param m1
     * @param m2
     * @param bizCodeEnum
     */
    public static void equals(Object m1, Object m2, BizCodeEnum bizCodeEnum) {
        if (!m1.equals(m2)) {
            log.info("not equals...............");
            throw new BusinessException(bizCodeEnum);
        }
    }

    /**
     * 断言参数不为空
     * 如果为空，则抛出异常
     *
     * @param s
     * @param bizCodeEnum
     */
    public static void notEmpty(String s, BizCodeEnum bizCodeEnum) {
        if (StringUtils.isEmpty(s)) {
            log.info("is empty...............");
            throw new BusinessException(bizCodeEnum);
        }
    }
}
