package com.indi.common.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component  // 被Spring自动管理，不需要去手动new
@Slf4j
public class MyMetaObjectHandler implements MetaObjectHandler {
    @Override
    public void insertFill(MetaObject metaObject) {
        // 实现填充业务逻辑
        // 参数：列的元数据信息 | 针对哪个属性 | 属性的类型 | 属性的值
//        log.info("insert 自动填充");
        this.setInsertFieldValByName("createTime", new Date(), metaObject);
        this.setInsertFieldValByName("updateTime", new Date(), metaObject);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
//        log.info("update 自动填充");
        this.setUpdateFieldValByName("updateTime", new Date(), metaObject);
    }
}

