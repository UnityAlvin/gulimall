package com.indi.common.to.es;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * 需要检索的sku模型
 */
@Data
public class SkuEsModelTO {
    private Long skuId;
    private Long spuId;
    private String skuTitle;
    private BigDecimal skuPrice;
    private String skuImg;
    private Long saleCount;
    /**
     * 是否有库存
     */
    private Boolean stockOrNot;
    /**
     * 热度
     */
    private Long hotScore;
    private Long brandId;
    private Long categoryId;
    private String brandName;
    private String brandImg;
    private String categoryName;
    private List<SkuAttrEsModelTO> attrs;
}
