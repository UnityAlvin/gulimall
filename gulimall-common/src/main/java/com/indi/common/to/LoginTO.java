package com.indi.common.to;

import lombok.Data;

@Data
public class LoginTO {
    private String userAccount;
    private String password;
}
