package com.indi.common.to.es;

import lombok.Data;

/**
 * 需要检索的规格属性
 */
@Data
public class SkuAttrEsModelTO {
    private Long attrId;
    private String attrName;
    private String attrValue;
}
