package com.indi.common.to;

import lombok.Data;

@Data
public class RegisterTO {
    private String userName;
    private String password;
    private String phoneNum;
}
