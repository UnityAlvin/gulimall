package com.indi.common.to;

import lombok.Data;

/**
 * 是否有库存
 */
@Data
public class SkuStockTO {
    private Long skuId;
    private Boolean stockOrNot;
}
