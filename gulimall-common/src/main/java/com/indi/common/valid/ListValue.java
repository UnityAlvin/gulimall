package com.indi.common.valid;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;


@Documented
@Constraint(validatedBy = ListValueConstraintValidator.class)  // 使用哪个校验器进行校验，这里不指定，就需要在初始化时指定
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })   // 注解的标注位置
@Retention(RUNTIME) // 注解的时机，表示在运行时获取到
public @interface ListValue {

    // 想要自定义校验注解，必须包含以下3个属性
    String message() default "{com.indi.common.valid.ListValue.message}"; // 错误信息的位置

    Class<?>[] groups() default { };    // 需要支持分组校验的功能

    Class<? extends Payload>[] payload() default { };   // 可以自定义一些负载信息

    int[] values() default {};  // 指定的值
}
