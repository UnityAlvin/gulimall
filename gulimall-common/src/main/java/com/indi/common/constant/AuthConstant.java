package com.indi.common.constant;

public class AuthConstant {
    public static final String SMS_CODE_PREFIX = "gulimall:sms:code:";
    public static final String SESSION_ATTR_NAME = "loginUser";
}
