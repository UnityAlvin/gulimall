package com.indi.common.constant;

/**
 * @author UnityAlvin
 * @date 2021/7/27 10:32
 */
public class SeckillConstant {
    // 秒杀场次的缓存key
    public static final String SESSIONS_CACHE_PREFIX = "gulimall:seckill:sessions:";

    // 秒杀场次关联的商品的缓存key
    public static final String SECKILL_SKU_PREFIX  = "gulimall:seckill:skus";

    // 秒杀商品的库存的缓存key
    public static final String SECKILL_SKU_STOCK_SEMAPHORE  = "gulimall:seckill:stock:";

    // 上架秒杀信息所需要的分布式锁
    public static final String UPLOAD_LOCK  = "gulimall:seckill:stock:";

    // 是否抢购过的标识
    public static final String SECKILL_ABSENT_PREFIX = "gulimall:seckill:absent:";

    public static final String SECKILL_EVENT_EXCHANGE = "seckill-event-exchange";    // 交换机
    public static final String SECKILL_RELEASE_STOCK_QUEUE = "seckill.release.stock.queue"; // 队列
    public static final String SECKILL_RELEASE_ROUTING_KEY = "seckill.release.#";   // 路由键
}
