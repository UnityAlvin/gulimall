package com.indi.common.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

public class ProductConstant {

    @Getter
    @AllArgsConstructor
    public enum AttrEnum {
        ATTR_TYPE_SALE(0, "销售属性"),
        ATTR_TYPE_BASE(1, "基本属性");

        private int code;
        private String message;
    }

    @Getter
    @AllArgsConstructor
    public enum ProductStatusEnum {
        SPU_NEW(0, "新建"),
        SPU_UP(1, "已上架"),
        SPU_DOWN(2, "已下架");

        private int code;
        private String message;
    }
}
