package com.indi.common.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

public class WareConstant {
    public static final String STOCK_EVENT_EXCHANGE = "stock-event-exchange";    // 交换机

    public static final String STOCK_DELAY_QUEUE = "stock.delay.queue";  // 队列1
    public static final String STOCK_RELEASE_STOCK_QUEUE = "stock.release.stock.queue";  // 队列2

    public static final String STOCK_LOCKED_ROUTING_KEY = "stock.locked";    // 路由键1
    public static final String STOCK_RELEASE_ROUTING_KEY = "stock.release";  // 路由键2

    public static final String STOCK_RELEASE_BINDING = "stock.release.#";    // 绑定关系2

    public static final Long MESSAGE_TTL = 120000L;  // 消息等待时间

    @Getter
    @AllArgsConstructor
    public enum LockStatusEnum {
        LOCKED(1, "已锁定"),
        UNLOCKED(2, "已解锁"),
        DEDUCTED(3, "已扣减");

        private int code;
        private String message;
    }

    @Getter
    @AllArgsConstructor
    public enum PurchaseEnum {
        UNALLOCATED(0, "未分配"),
        ASSIGNED(1, "已分配"),
        RECEIVED(2, "已领取"),
        FINISHED(3, "已完成"),
        UNUSUAL(4, "有异常");

        private int code;
        private String message;
    }

    @Getter
    @AllArgsConstructor
    public enum PurchaseDetailEnum {
        UNALLOCATED(0, "未分配"),
        ASSIGNED(1, "已分配"),
        PURCHASING(2, "正在采购"),
        FINISHED(3, "已完成"),
        PURCHASE_FAILED(4, "采购失败");

        private int code;
        private String message;
    }
}
