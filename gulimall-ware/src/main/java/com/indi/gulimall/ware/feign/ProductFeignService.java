package com.indi.gulimall.ware.feign;

import com.indi.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient("gulimall-product")
public interface ProductFeignService {
    @GetMapping("/admin/product/sku-info/info/{skuId}")
    R info(@PathVariable("skuId") Long skuId);
}
