package com.indi.gulimall.ware.vo;

import lombok.Data;

import java.util.List;

@Data
public class PurchaseFinishVO {
    private Long id;    // 采购单id
    private List<PurchaseDetailFinishVO> items; // 采购需求
}
