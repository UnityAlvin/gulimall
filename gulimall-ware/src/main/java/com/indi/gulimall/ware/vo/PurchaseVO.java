package com.indi.gulimall.ware.vo;

import lombok.Data;

import java.util.List;

@Data
public class PurchaseVO {
    private Long purchaseId;    // 整单id
    private List<Long> items;   // 要合并的采购需求id
}
