package com.indi.gulimall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.indi.common.dto.OrderDTO;
import com.indi.common.dto.WareLockStockDTO;
import com.indi.common.dto.mq.StockLockDTO;
import com.indi.common.to.SkuStockTO;
import com.indi.common.utils.PageUtils;
import com.indi.gulimall.ware.entity.WareSkuEntity;

import java.util.List;
import java.util.Map;

/**
 * 商品库存
 *
 * @author UnityAlvin
 * @email UnityAlvin@qq.com
 * @date 2021-05-17 09:43:04
 */
public interface WareSkuService extends IService<WareSkuEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveOrUpdateWareSku(Long skuId, Long wareId, Integer skuNum);

    List<SkuStockTO> getStockBySkuIds(List<Long> skuIds);

    /**
     * 锁定下单商品的库存
     * @param wareLockStockDTO
     * @return
     */
    void lockStock(WareLockStockDTO wareLockStockDTO);

    /**
     * 自动解锁库存
     * @param stockLockDTO
     */
    void unlockStock(StockLockDTO stockLockDTO);

    /**
     * 主动解锁库存
     * @param orderDTO
     */
    void unlockStock(OrderDTO orderDTO);
}

