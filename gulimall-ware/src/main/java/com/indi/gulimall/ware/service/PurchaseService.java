package com.indi.gulimall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.indi.common.utils.PageUtils;
import com.indi.common.utils.R;
import com.indi.gulimall.ware.entity.PurchaseEntity;
import com.indi.gulimall.ware.vo.PurchaseFinishVO;
import com.indi.gulimall.ware.vo.PurchaseVO;

import java.util.List;
import java.util.Map;

/**
 * 采购信息
 *
 * @author UnityAlvin
 * @email UnityAlvin@qq.com
 * @date 2021-05-17 09:43:04
 */
public interface PurchaseService extends IService<PurchaseEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils getUnclaimedList(Map<String, Object> params);

    R mergePurchase(PurchaseVO purchaseVO);

    void receivePurchase(List<Long> purchaseIds);

    void finishPurchase(PurchaseFinishVO purchaseFinishVO);
}

