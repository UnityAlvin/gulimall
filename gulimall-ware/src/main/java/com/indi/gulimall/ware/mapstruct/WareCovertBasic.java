package com.indi.gulimall.ware.mapstruct;

import com.indi.common.dto.mq.StockLockDTO.StockLockDetailDTO;
import com.indi.gulimall.ware.entity.WareOrderTaskDetailEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @author UnityAlvin
 * @date 2021/7/23 16:42
 */
@Mapper(componentModel = "spring")
public interface WareCovertBasic {
    WareCovertBasic INSTANCE = Mappers.getMapper(WareCovertBasic.class);

    StockLockDetailDTO wareOrderTaskDetailToStockLockDetailDTO(WareOrderTaskDetailEntity wareOrderTaskDetail);
}
