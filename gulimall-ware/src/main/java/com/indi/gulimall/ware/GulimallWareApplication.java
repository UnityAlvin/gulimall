package com.indi.gulimall.ware;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableRabbit
@EnableDiscoveryClient
@SpringBootApplication
@ComponentScan({"com.indi.common","com.indi.gulimall.ware"})
@EnableTransactionManagement
@EnableFeignClients(basePackages = "com.indi.gulimall.ware.feign")
public class GulimallWareApplication {
    public static void main(String[] args) {
        SpringApplication.run(GulimallWareApplication.class, args);
    }
}