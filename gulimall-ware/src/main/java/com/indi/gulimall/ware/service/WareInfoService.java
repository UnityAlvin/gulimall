package com.indi.gulimall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.indi.common.utils.PageUtils;
import com.indi.gulimall.ware.entity.WareInfoEntity;

import java.util.Map;

/**
 * 仓库信息
 *
 * @author UnityAlvin
 * @email UnityAlvin@qq.com
 * @date 2021-05-17 09:43:04
 */
public interface WareInfoService extends IService<WareInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

