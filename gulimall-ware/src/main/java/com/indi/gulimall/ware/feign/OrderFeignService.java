package com.indi.gulimall.ware.feign;

import com.indi.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author UnityAlvin
 * @date 2021/7/23 19:15
 */
@FeignClient("gulimall-order")
public interface OrderFeignService {
    /**
     * 根据订单编号查询订单信息
     * @param orderSn
     * @return
     */
    @GetMapping("/web/order/order-by-order-sn/{orderSn}")
    R infoByOrderSn(@PathVariable("orderSn") String orderSn);
}
