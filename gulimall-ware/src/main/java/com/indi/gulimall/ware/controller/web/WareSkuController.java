package com.indi.gulimall.ware.controller.web;

import com.indi.common.dto.WareLockStockDTO;
import com.indi.common.utils.R;
import com.indi.gulimall.ware.service.WareSkuService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 商品库存
 *
 * @author UnityAlvin
 * @email UnityAlvin@qq.com
 * @date 2021-05-17 09:43:04
 */
@Api(tags = "商品库存")
@RestController
@RequestMapping("/web/ware/ware-sku")
public class WareSkuController {
    @Autowired
    private WareSkuService wareSkuService;

    /**
     * 为下单商品锁定库存
     *
     * @param wareLockStockDTO
     * @return
     */
    @PostMapping("/lockStock")
    public R lockStock(@RequestBody WareLockStockDTO wareLockStockDTO) {
        wareSkuService.lockStock(wareLockStockDTO);
        return R.ok();
    }
}


