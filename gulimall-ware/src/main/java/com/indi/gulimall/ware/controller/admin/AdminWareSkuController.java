package com.indi.gulimall.ware.controller.admin;

import com.indi.common.to.SkuStockTO;
import com.indi.common.utils.PageUtils;
import com.indi.common.utils.R;
import com.indi.gulimall.ware.entity.WareSkuEntity;
import com.indi.gulimall.ware.service.WareSkuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * 商品库存
 *
 * @author UnityAlvin
 * @email UnityAlvin@qq.com
 * @date 2021-05-17 09:43:04
 */
@Api(tags = "商品库存管理")
@RestController
@RequestMapping("/admin/ware/ware-sku")
public class AdminWareSkuController {
    @Autowired
    private WareSkuService wareSkuService;

    /**
     * 查询商品是否有库存
     * @param skuIds
     * @return
     */
    @ApiOperation("查询是商品否有库存")
    @PostMapping("/stock-or-not")
    public R stockOrNot(@RequestBody List<Long> skuIds){
        List<SkuStockTO> skuStockTOs = wareSkuService.getStockBySkuIds(skuIds);
        return R.ok().setData(skuStockTOs);
    }

    /**
     * 列表
     */
    @GetMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = wareSkuService.queryPage(params);
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
        WareSkuEntity wareSku = wareSkuService.getById(id);
        return R.ok().put("wareSku", wareSku);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public R save(@RequestBody WareSkuEntity wareSku){
        wareSkuService.save(wareSku);
        return R.ok();
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public R update(@RequestBody WareSkuEntity wareSku){
        wareSkuService.updateById(wareSku);
        return R.ok();
    }

    /**
     * 删除
     */
    @DeleteMapping("/delete")
    public R delete(@RequestBody Long[] ids){
        wareSkuService.removeByIds(Arrays.asList(ids));
        return R.ok();
    }

}


