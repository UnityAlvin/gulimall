package com.indi.gulimall.ware.vo;

import lombok.Data;

@Data
public class PurchaseDetailFinishVO {
    private Long itemId;    // 采购需求的id
    private Integer status; // 采购需求的状态
    private String reason;  // 采购失败的原因
}
