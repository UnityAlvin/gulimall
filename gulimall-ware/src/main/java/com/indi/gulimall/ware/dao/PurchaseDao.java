package com.indi.gulimall.ware.dao;

import com.indi.gulimall.ware.entity.PurchaseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 * 
 * @author UnityAlvin
 * @email UnityAlvin@qq.com
 * @date 2021-05-17 09:43:04
 */
@Mapper
public interface PurchaseDao extends BaseMapper<PurchaseEntity> {
	
}
