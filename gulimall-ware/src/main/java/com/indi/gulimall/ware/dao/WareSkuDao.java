package com.indi.gulimall.ware.dao;

import com.indi.gulimall.ware.entity.WareSkuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.indi.gulimall.ware.entity.result.WareSkuStockCountResult;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 商品库存
 * 
 * @author UnityAlvin
 * @email UnityAlvin@qq.com
 * @date 2021-05-17 09:43:04
 */
@Mapper
public interface WareSkuDao extends BaseMapper<WareSkuEntity> {

    void updateStock(@Param("skuId") Long skuId, @Param("skuNum") Integer skuNum, @Param("wareId") Long wareId);

    List<WareSkuStockCountResult> getStockBySkuIds(@Param("skuIds") List<Long> skuIds);

    List<Long> wareIdList(@Param("skuId") Long skuId, @Param("skuQuantity") Integer skuQuantity);

    int lockStock(@Param("skuId") Long skuId, @Param("wareId") Long wareId, @Param("skuQuantity") Integer skuQuantity);

    void unlockStock(@Param("skuId") Long skuId, @Param("wareId") Long wareId, @Param("skuNum") Integer skuNum);
}
