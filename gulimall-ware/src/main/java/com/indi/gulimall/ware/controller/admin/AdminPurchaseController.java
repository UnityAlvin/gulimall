package com.indi.gulimall.ware.controller.admin;

import com.indi.common.utils.PageUtils;
import com.indi.common.utils.R;
import com.indi.gulimall.ware.entity.PurchaseEntity;
import com.indi.gulimall.ware.service.PurchaseService;
import com.indi.gulimall.ware.vo.PurchaseFinishVO;
import com.indi.gulimall.ware.vo.PurchaseVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * 采购信息
 *
 * 完整的采购流程：
 * 创建采购需求 ->
 * 合并采购单 ->
 *      如果选择的采购单有采购员，合并之后，将继续使用原有采购员
 *      如果选择的采购单，没有分配采购员，需要合并完之后，-> 分配采购员
 * 采购员领取采购单 -> 完成采购
 *
 * @author UnityAlvin
 * @email UnityAlvin@qq.com
 * @date 2021-05-17 09:43:04
 */
@Api(tags = "采购单管理")
@RestController
@RequestMapping("/admin/ware/purchase")
public class AdminPurchaseController {
    @Autowired
    private PurchaseService purchaseService;

    /**
     * 完成采购
     * @param purchaseFinishVO
     * @return
     */
    @PostMapping("/finish")
    @ApiOperation("完成采购")
    public R finish(@RequestBody PurchaseFinishVO purchaseFinishVO){
        purchaseService.finishPurchase(purchaseFinishVO);
        return R.ok();
    }

    /**
     * 领取采购单
     * @param purchaseIds
     * @return
     */
    @PostMapping("/receive")
    @ApiOperation("领取采购单")
    public R receive(@RequestBody List<Long> purchaseIds){
        purchaseService.receivePurchase(purchaseIds);
        return R.ok();
    }

    /**
     * 将未分配/已分配的采购需求，合并为一个采购单
     * @param purchaseVO
     * @return
     */
    @PostMapping("/merge")
    public R merge(@RequestBody PurchaseVO purchaseVO){
        return purchaseService.mergePurchase(purchaseVO);
    }

    /**
     * 查询未领取的采购单
     * @param params
     * @return
     */
    @GetMapping("/unclaimed/list")
    public R getUnclaimedList(@RequestParam Map<String, Object> params){
        PageUtils page = purchaseService.getUnclaimedList(params);
        return R.ok().put("page", page);
    }

    /**
     * 列表
     */
    @GetMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = purchaseService.queryPage(params);
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
		PurchaseEntity purchase = purchaseService.getById(id);
        return R.ok().put("purchase", purchase);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public R save(@RequestBody PurchaseEntity purchase){
		purchaseService.save(purchase);
        return R.ok();
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public R update(@RequestBody PurchaseEntity purchase){
		purchaseService.updateById(purchase);
        return R.ok();
    }

    /**
     * 删除
     */
    @DeleteMapping("/delete")
    public R delete(@RequestBody Long[] ids){
		purchaseService.removeByIds(Arrays.asList(ids));
        return R.ok();
    }

}
