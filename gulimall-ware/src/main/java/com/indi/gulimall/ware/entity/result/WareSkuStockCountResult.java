package com.indi.gulimall.ware.entity.result;

import lombok.Data;

@Data
public class WareSkuStockCountResult {
    private Long skuId;
    private Long stockCount;
}
