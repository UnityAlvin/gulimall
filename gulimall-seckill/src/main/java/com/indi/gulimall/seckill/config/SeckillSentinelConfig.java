package com.indi.gulimall.seckill.config;

import com.alibaba.csp.sentinel.adapter.servlet.callback.WebCallbackManager;
import com.alibaba.fastjson.JSON;
import com.indi.common.enums.BizCodeEnum;
import com.indi.common.utils.R;
import org.springframework.context.annotation.Configuration;

/**
 * @author UnityAlvin
 * @date 2021/8/1 9:49
 */
@Configuration
public class SeckillSentinelConfig {
    /**
     * 自定义秒杀流控响应
     */
    public SeckillSentinelConfig(){
        WebCallbackManager.setUrlBlockHandler((request, response, e) ->{
            R error = R.error(BizCodeEnum.SMS_CODE_EXCEPTION.getCode(), BizCodeEnum.SMS_CODE_EXCEPTION.getMessage());
            response.setCharacterEncoding("UTF-8");
            response.setContentType("application/json");
            response.getWriter().write(JSON.toJSONString(error));
        });
    }
}
