package com.indi.gulimall.seckill.scheduled;

/**
 * 测试定时任务
 * 默认是使用的自己的
 *
 * @author UnityAlvin
 * @date 2021/7/26 19:42
 */
//@Slf4j
//@Component
//@EnableScheduling   // 开启定时功能
//@EnableAsync    // 开启异步任务功能
public class HelloSchedule {

    /**
     *  一、与Quarts的区别
     *      1、Spring中的定时任务由6位组成，不支持第7位的年
     *      2、第6位的数字格式，1-7，代表周一到周日，当然也可以写成MON-SUN
     *      3、其它普遍与Quarts一致
     *
     *  二、定时任务默认是阻塞的
     *      只要当前任务没执行完，下一个任务就执行不了
     */
//    @Scheduled(cron = "*/5 * * ? * 1") // 开启定时任务
//    public void hello(){
//        log.info("hello");
//    }

    /**
     * 三、使用定时任务+异步任务解决阻塞问题
     *
     * @throws InterruptedException
     */
//    @Scheduled(cron = "* * * ? * 1")
//    @Async  // 执行异步任务
//    public void block() throws InterruptedException {
//        log.info("hello......");
//        Thread.sleep(3000);
//    }
}
