package com.indi.gulimall.seckill.config;

import com.indi.common.constant.SeckillConstant;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.Binding.DestinationType;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {

    /**
     * 将对象序列化成JSON
     *
     * @return
     */
    @Bean
    public MessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public Exchange exchange() {
        return new TopicExchange(SeckillConstant.SECKILL_EVENT_EXCHANGE, true, false);
    }

    @Bean
    public Queue seckillReleaseStockQueue() {
        return new Queue(SeckillConstant.SECKILL_RELEASE_STOCK_QUEUE, true, false, false);
    }

    @Bean
    public Binding stockReleaseBinding() {
        return new Binding(SeckillConstant.SECKILL_RELEASE_STOCK_QUEUE, DestinationType.QUEUE, SeckillConstant.SECKILL_EVENT_EXCHANGE,
                SeckillConstant.SECKILL_RELEASE_ROUTING_KEY, null);
    }
}
