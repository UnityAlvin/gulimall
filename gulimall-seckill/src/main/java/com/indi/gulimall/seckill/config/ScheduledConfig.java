package com.indi.gulimall.seckill.config;

import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 秒杀任务配置
 * @author UnityAlvin
 * @date 2021/7/27 9:05
 */
@EnableAsync
@EnableScheduling
public class ScheduledConfig {
}
