package com.indi.gulimall.seckill.controller.web;

import com.indi.common.dto.mq.SeckillSkuDTO;
import com.indi.common.utils.R;
import com.indi.common.dto.SeckillSkuRedisDTO;
import com.indi.gulimall.seckill.service.SeckillService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author UnityAlvin
 * @date 2021/7/28 8:38
 */
@Controller
public class SeckillController {
    @Resource
    private SeckillService seckillService;

    /**
     * 获取当前时间应该秒杀的商品
     *
     * @return
     */
    @GetMapping("/web/seckill/current-seckill-skus")
    @ResponseBody
    public R getCurrentSeckillSkus() {
        List<SeckillSkuRedisDTO> seckillSkuRedisDTOs = seckillService.getCurrentSeckillSkus();
        return R.ok().setData(seckillSkuRedisDTOs);
    }

    /**
     * 根据skuId获取对应的秒杀信息
     *
     * @return
     */
    @GetMapping("/web/seckill/seckill-sku-relation/{skuId}")
    @ResponseBody
    public R getSeckillSkuRelation(@PathVariable("skuId") Long skuId) {
        SeckillSkuRedisDTO relationDTO = seckillService.getSeckillSkuRelation(skuId);
        return R.ok().setData(relationDTO);
    }

    /**
     * 秒杀
     *
     * @return
     */
    @GetMapping("/web/seckill/kill")
    public String kill(@RequestParam("killId") String killId,
                       @RequestParam("randomCode") String randomCode,
                       @RequestParam("num") Integer num,
                       Model model) {
        SeckillSkuDTO seckillSkuDTO = seckillService.kill(killId, randomCode, num);
        model.addAttribute("seckillSkuInfo", seckillSkuDTO);
        return "seckill-result";
    }
}
