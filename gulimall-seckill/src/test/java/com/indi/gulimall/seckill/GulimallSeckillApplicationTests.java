package com.indi.gulimall.seckill;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GulimallSeckillApplicationTests {
    /**
     * 查询3天之内的时间区间
     */
    @Test
    public void contextLoads() {
        LocalDate today = LocalDate.now();  // 当前日期 年 月 日
        LocalTime minTime = LocalTime.MIN;  // 最小时间 00:00:00

        LocalDate twoDaysLaterDate = today.plusDays(2); // 两天之后的日期 年月日
        LocalTime maxTime = LocalTime.MAX;  // 最大时间 23:59:59

        // 将日期和时间组合起来，并转换成想要的格式
        String beginDate = LocalDateTime.of(today, minTime).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        String endDate = LocalDateTime.of(twoDaysLaterDate, maxTime).format(DateTimeFormatter
                .ofPattern("yyyy-MM-dd HH:mm:ss"));

        System.out.println("开始时间：" + beginDate);
        System.out.println("结束时间：" + endDate);
    }
}
